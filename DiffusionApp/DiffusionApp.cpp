// DiffusionApp.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include "methodics.h"
#include "diffusionlib.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>


int main(int argc, char *argv[])
{
	if (argc != 8) {
		fprintf(stderr, "There must be 8 arguments (%d was given):\n", argc);
		fprintf(stderr, "* name of executable (DiffusionApp.exe)\n");
		fprintf(stderr, "* diffusion coefficient\n");
		fprintf(stderr, "* name of boundary condition type\n");
		fprintf(stderr, "* name of initial file\n");
		fprintf(stderr, "* value of initial timepoint\n");
		fprintf(stderr, "* number of timesteps\n");
		fprintf(stderr, "* value of final timepoint\n");
		fprintf(stderr, "* name of result-file\n");
		return 1;
	}
	char *sD = argv[1], *bc = argv[2], *initFile = argv[3], *sTinit = argv[4], *sNt = argv[5], *sTfin = argv[6], *resFile = argv[7], *fullPath;
	double d = atof(sD);
	size_t nt = atoi(sNt);
	double t_init = atof(sTinit), t_fin = atof(sTfin);
	if (nt == 0) {
		fprintf(stderr, "Invalid number of time-steps: %s", sNt);
		return 2;
	}
	Datagrid grid;
	fullPath = _fullpath(NULL, initFile, FILENAME_MAX);
	fprintf(stderr, "Loading file %s...\n", fullPath ? fullPath : initFile);
	if (load(initFile, grid)) {
		fprintf(stderr, "Unable to load file!\n");
		return 3;
	}
	fprintf(stderr, "File loaded successfully.\n");
	free(fullPath);
	if (!strcmp(bc, "fixedbc")) {
		const void* params[1]{ &d };
		fprintf(stderr, "Runninc CNe method with fixed boundary condition, using OpenCL...\n");
		if (euler(grid, (CLParametrizer)CLDiffusion::CNe::parametrizeFixedBC, params, nt, t_init, t_fin)) {
			fprintf(stderr, "A runtime error occurred during calculation\n");
			return 4;
		}
		fprintf(stderr, "Calculation done\n");
	}
	else if (!strcmp(bc, "periodic")) {
		fprintf(stderr, "Running Euler's method with periodic boundary condition...\n");
		fprintf(stderr, "Warning: There is no OpenCL-based implementation to this type of boundary condition. Defaulting back to legacy method.\n");
		if (euler(grid.data, Diffusion::Periodic(d, grid.scaling), nt, t_init, t_fin)) {
			fprintf(stderr, "A runtime error occurred during calculation\n");
			return 4;
		}
		fprintf(stderr, "Calculation done\n");
	}
	else if (!strcmp(bc, "isolated")) {
		fprintf(stderr, "Running Euler's method with isolated boundary condition...\n");
		fprintf(stderr, "Warning: There is no OpenCL-based implementation to this type of boundary condition. Defaulting back to legacy method.\n");
		if (euler(grid.data, Diffusion::Isolated(d, grid.scaling), nt, t_init, t_fin)) {
			fprintf(stderr, "A runtime error occurred during calculation\n");
			return 4;
		}
		fprintf(stderr, "Calculation done\n");
	}
	else {
		fprintf(stderr, "Unknown boundary condition: %s\n", bc);
		fprintf(stderr, "Only 'fixedbc', 'periodic' and 'isolated' are allowed\n");
		return 2;
	}
	fullPath = _fullpath(NULL, resFile, FILENAME_MAX);
	fprintf(stderr, "Saving result to: %s...\n", fullPath ? fullPath : resFile);
	if (save(resFile, grid)) {
		fprintf(stderr, "Unable to write file!\n");
		return 5;
	}
	fprintf(stderr, "Result saved successfully.\n");
	free(fullPath);
	return 0;
}
