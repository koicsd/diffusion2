#pragma once

#include <iostream>
#include <vector>
#include <CL/opencl.h>
using std::vector;

#define MAX_ENTITY_NO 3
#define MAX_TEXT_LENGTH 255


struct FloatHelper {
private:
	void* _ptr = NULL;
	size_t _size = 0;
	cl_double _value = 0;
	cl_float _narrowed = 0;

public:
	void* const& ptr;
	const size_t& size;
	FloatHelper() : ptr(_ptr), size(_size) {};
	cl_double value(cl_double);
	cl_double value();
};

struct Float2Helper {
private:
	void* _ptr = NULL;
	size_t _size = 0;
	cl_double2 _value{ 0, 0 };
	cl_float2 _narrowed{ 0, 0 };

public:
	void * const& ptr;
	const size_t& size;
	Float2Helper() : ptr(_ptr), size(_size) {};
	cl_double2 value(cl_double2);
	cl_double2 value();
};

struct Float3Helper {
private:
	void* _ptr = NULL;
	size_t _size = 0;
	cl_double3 _value{ 0,0,0 };
	cl_float3 _narrowed{ 0,0,0 };

public:
	void * const& ptr;
	const size_t& size;
	Float3Helper() : ptr(_ptr), size(_size) {};
	cl_double3 value(cl_double3);
	cl_double3 value();
};

struct FloatArrayHelper {
private:
	void* _ptr;
	size_t _size;
	vector<double> _wide;
	vector<float> _narrowed;

public:
	void * const& ptr;
	const size_t& size;
	FloatArrayHelper(const double* arr, unsigned int size);
	void readback(double* arr);
};


bool isFloat64Supported();
void printErr(const char* operation, cl_int err);
bool initCL(cl_context&, cl_command_queue&);
bool compileCLProgramFromString(cl_uint source_size, const char** source, cl_context context, cl_program& program);
bool compileCLFile(cl_context context, const char* filename, cl_program& program);
bool createMemBuffCopyHost(cl_context context, size_t size, void* init_data, cl_mem& buf, bool devWriteEna = true, bool hostReadbackEna = true);
bool createMemBuffFromDevToHost(cl_context context, size_t size, cl_mem& buf, bool devReadbackEna = true, bool hostWriteEna = false);
bool feedData(cl_command_queue queue, const void* data, size_t size, cl_mem buf);
bool enqueueCopyBuffer(cl_command_queue queue, cl_mem src, cl_mem dest, size_t size,
	size_t src_offs = 0, size_t dest_offs = 0, vector<cl_event> *evnts2wait = NULL);
bool getKernelWithArgs(cl_program program, const char* name, cl_kernel& kernel, size_t argc, ...);
bool enqueueKernel(cl_command_queue, cl_kernel, size_t dim, const size_t* offset, const size_t* n_cells,
	vector<cl_event> *evnts2wait = NULL);
bool getExecutionStatus(cl_event evnt, cl_int&);
bool readResult(cl_command_queue queue, cl_mem buffer, size_t size, void* data);
bool releaseResources();
bool deinitCL();
