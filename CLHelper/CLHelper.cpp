// CLHelper.cpp : Defines the functions for the static library.
//

#include "pch.h"
#include "framework.h"
#include "CLHelper.h"

#include <CL/opencl.h>
#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>
#include <cstdarg>
#include <filesystem>
#ifdef _WIN32
#include <windows.h>
#elif
#include <unistd.h>
#endif
using std::cerr;
using std::endl;
using std::string;
using std::filesystem::path;
using std::filesystem::absolute;
using std::filesystem::exists;
using std::filesystem::current_path;
using std::getenv;


#define MAX_TEXT_LENGTH 255
cl_platform_id _platform;
cl_device_id _device;
bool _float64support = false;
cl_context _context;
cl_command_queue _queue;
vector<cl_program> _progsBuilt;
vector<cl_mem> _mem2release;
vector<cl_kernel> _kernels2release;
void printFloatConfig(cl_device_fp_config config);
bool createContext(cl_device_id device, cl_context& context);
bool createQueue(cl_device_id device, cl_context context, cl_command_queue& queue);
bool printBuildLog(cl_program, cl_device_id);
bool releaseKernel(cl_kernel&);
bool releaseMem(cl_mem&);
bool releaseQueue(cl_command_queue&);
bool releaseProgram(cl_program&);
bool releaseContext(cl_context&);
bool releaseDevice(cl_device_id&);

cl_double FloatHelper::value(cl_double value) {
	_value = value;
	if (_float64support) {
		_ptr = &_value;
		_size = sizeof(_value);
	}
	else {
		_narrowed = (cl_float)value;
		_ptr = &_narrowed;
		_size = sizeof(_narrowed);
	}
	return _value;
}

cl_double FloatHelper::value() {
	if (!_float64support)
		_value = _narrowed;
	return _value;
}

cl_double2 Float2Helper::value(cl_double2 value) {
	_value = value;
	if (_float64support) {
		_ptr = &_value;
		_size = sizeof(_value);
	}
	else {
		_narrowed.x = (cl_float)value.x;
		_narrowed.y = (cl_float)value.y;
		_ptr = &_narrowed;
		_size = sizeof(_narrowed);
	}
	return _value;
}

cl_double2 Float2Helper::value() {
	if (!_float64support) {
		_value.x = _narrowed.x;
		_value.y = _narrowed.y;
	}
	return _value;
}

cl_double3 Float3Helper::value(cl_double3 value) {
	_value = value;
	if (_float64support) {
		_ptr = &_value;
		_size = sizeof(_value);
	}
	else {
		_narrowed.x = (cl_float)value.x;
		_narrowed.y = (cl_float)value.y;
		_narrowed.z = (cl_float)value.z;
		_ptr = &_narrowed;
		_size = sizeof(_narrowed);
	}
	return _value;
}

cl_double3 Float3Helper::value() {
	if (!_float64support) {
		_value.x = _narrowed.x;
		_value.y = _narrowed.y;
		_value.z = _narrowed.z;
	}
	return _value;
}

FloatArrayHelper::FloatArrayHelper(const double* arr, unsigned int size) : ptr(_ptr), size(_size) {
	if (_float64support) {
		_wide.reserve(size);
		_wide.assign<const double*>(arr, arr + size);
		_ptr = _wide.data();
		_size = _wide.size() * sizeof(double);
	}
	else {
		_narrowed.reserve(size);
		_narrowed.assign<const double*>(arr, arr + size);
		_ptr = _narrowed.data();
		_size = _narrowed.size() * sizeof(float);
	}
}

void FloatArrayHelper::readback(double* arr) {
	if (!_float64support) {
		_wide.reserve(_narrowed.size());
		_wide.assign(_narrowed.cbegin(), _narrowed.cend());
	}
	if (arr == _wide.data()) return;
	memcpy(arr, _wide.data(), _wide.size() * sizeof(double));
}

bool getFirstPlatform(cl_platform_id& platform) {
	cl_uint count;
	clGetPlatformIDs(1, &platform, &count);
	if (!count) {
		cerr << "No platform installed!" << endl;
		return false;
	}
	return true;
}

bool getFirstGPUorAnyDevice(cl_platform_id platform, cl_device_id& device, bool& float64support) {
	cl_uint count;
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, &count);
	if (!count) {
		clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, &count);
		if (!count) {
			cerr << "No device found!" << endl;
			return false;
		}
		cerr << "No GPU found!" << endl;
	}
	cerr << "Device being used:" << endl;
	char name[MAX_TEXT_LENGTH] = "<NA>", vendor[MAX_TEXT_LENGTH] = "<NA>", version[MAX_TEXT_LENGTH] = "<NA>";
	cl_device_type type; cl_device_fp_config single_config, double_config;
	clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(cl_device_type), &type, NULL);
	clGetDeviceInfo(device, CL_DEVICE_NAME, MAX_TEXT_LENGTH, name, NULL);
	clGetDeviceInfo(device, CL_DEVICE_VENDOR, MAX_TEXT_LENGTH, vendor, NULL);
	clGetDeviceInfo(device, CL_DEVICE_VERSION, MAX_TEXT_LENGTH, version, NULL);
	clGetDeviceInfo(device, CL_DEVICE_SINGLE_FP_CONFIG, sizeof(cl_device_fp_config), &single_config, NULL);
	clGetDeviceInfo(device, CL_DEVICE_DOUBLE_FP_CONFIG, sizeof(cl_device_fp_config), &double_config, NULL);
	float64support = double_config ? true : false;
	cerr << "\tname:\t\t" << name << endl;
	cerr << "\ttype:\t\t";
	if (type & CL_DEVICE_TYPE_DEFAULT) cerr << "DEFAULT ";
	if (type & CL_DEVICE_TYPE_CPU) cerr << "CPU ";
	if (type & CL_DEVICE_TYPE_GPU) cerr << "GPU ";
	if (type & CL_DEVICE_TYPE_ACCELERATOR) cerr << "ACCELERATOR ";
	if (type & CL_DEVICE_TYPE_CUSTOM) cerr << "CUSTOM ";
	cerr << endl;
	cerr << "\tvendor:\t\t" << vendor << endl;
	cerr << "\tversion:\t" << version << endl;
	cerr << "\tsingle support:" << endl;
	printFloatConfig(single_config);
	cerr << "\tdouble suppport:" << endl;
	printFloatConfig(double_config);
	cerr << endl;
	return true;
}

void printFloatConfig(cl_device_fp_config config) {
	if (!config) cerr << "\t\tNO SUPPORT" << endl;
	if (config & CL_FP_DENORM) cerr << "\t\tDENORM" << endl;
	if (config & CL_FP_INF_NAN) cerr << "\t\tINF NAN" << endl;
	if (config & CL_FP_ROUND_TO_NEAREST) cerr << "\t\tROUND TO NEAREST" << endl;
	if (config & CL_FP_ROUND_TO_ZERO) cerr << "\t\tROUND TO ZERO" << endl;
	if (config & CL_FP_ROUND_TO_INF) cerr << "\t\tROUND TO INF" << endl;
	if (config & CL_FP_FMA) cerr << "\t\tFUSED MULTIPLE ADD" << endl;
}

bool createContext(cl_device_id device, cl_context& context) {
	cl_int err;
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	if (err != CL_SUCCESS) {
		printErr("creating context", err);
		return false;
	}
	return true;
}

bool createQueue(cl_device_id device, cl_context context, cl_command_queue& queue) {
	cl_int err;
	queue = clCreateCommandQueueWithProperties(context, device, NULL, &err);
	if (err != CL_SUCCESS) {
		printErr("creating command-queue for the device", err);
		return false;
	}
	return true;
}

bool initCL(cl_context& context, cl_command_queue& queue) {
	bool success = (_platform || getFirstPlatform(_platform))
		&& (_device || getFirstGPUorAnyDevice(_platform, _device, _float64support))
		&& (_context || createContext(_device, _context))
		&& (_queue || createQueue(_device, _context, _queue));
	context = _context;
	queue = _queue;
	return success;
}

bool isFloat64Supported() {
	return _float64support;
}

bool compileCLProgramFromString(cl_uint source_size, const char** source, cl_context context, cl_program& program) {
	cl_int err;
	program = clCreateProgramWithSource(context, source_size, source, NULL, &err);
	if (err != CL_SUCCESS) {
		printErr("creating Program object", err);
		return false;
	}
	err = clBuildProgram(program, 1, &_device, NULL, NULL, NULL);
	printBuildLog(program, _device);
	if (err != CL_SUCCESS) {
		printErr("building kernel-program", err);
		return false;
	}
	_progsBuilt.push_back(program);
	return true;
}

bool printBuildLog(cl_program program, cl_device_id device) {
	size_t log_size;
	if (clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size) != CL_SUCCESS)
		return false;
	char* build_log = new char[log_size];
	if (clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size, build_log, NULL) != CL_SUCCESS) {
		delete[] build_log;
		return false;
	}
	cerr << build_log << endl;
	delete[] build_log;
	return true;
}

path findOnPath(string filename) {
	// As .cl files are treated rather simple text files rather than program file by the OS,
	// they cannot be found from env. variable %path%. Therefore, we need some hooke...
	// #@!? the system.
	
	string PATH;
	
#if _WIN32
	{
		char* buf;
		size_t len;
		_dupenv_s(&buf, &len, "PATH");
		if (!buf || !len)
			return filename;
		PATH = buf;
		free(buf);
	}
	const char delim = ';';
#else
	PATH = getenv("PATH");
	const char delim = ':';
#endif
	size_t prev = 0;
	path path2file;
	for (size_t i = PATH.find(delim); i != string::npos; i = PATH.find(delim, prev)) {
		path2file = PATH.substr(prev, i - prev);
		path2file /= filename;
		if (exists(path2file))
			return path2file;
		prev = i + 1;
	}
	path2file = current_path() / filename;
	if (exists(path2file))
		return path2file;
	return filename;
}

/*path getExeDirectory() {
	// stolen from: https://stackoverflow.com/questions/50889647/best-way-to-get-exe-folder-path
#ifdef _WIN32
	// Windows specific
	wchar_t szPath[MAX_PATH];
	GetModuleFileNameW(NULL, szPath, MAX_PATH);
#else
	// Linux specific
	char szPath[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", szPath, PATH_MAX);
	if (count < 0 || count >= PATH_MAX)
		return {}; // some error
	szPath[count] = '\0';
#endif
	return std::filesystem::path{ szPath }.parent_path() / ""; // to finish the folder path with (back)slash
}*/

bool compileCLFile(cl_context context, const char* filename, cl_program& program) {
	
	path path2file = findOnPath(filename);
	std::ifstream file(path2file, std::ios_base::in);
	std::vector<std::string> lines;
	while (file.good()) {
		std::string line;
		std::getline(file, line);
		if (!file.eof()) line.append("\n");  // Is it platform-independent?
		// Egy�ltal�n ki a #@!? tal�lta ki,hogy a clCreateProgramWithSource soronk�nt k�l�n t�mbelembe szedve v�rja a forr�st (char**),
		// de m�gse tud k�l�nbs�get tenni sorok k�z�tt, ha nincs explicit a sorok v�g�n a delimiter?!
		lines.push_back(line);
	}
	if (file.bad()) {
		cerr << "Error while reading file: " << path2file.string() << endl;
		return false;
	}
	std::vector<const char*> c_lines;
	c_lines.reserve(lines.size());
	for (const std::string& line : lines)
	{
		c_lines.push_back(line.c_str());
	}
	return compileCLProgramFromString(c_lines.size(), c_lines.data(), context, program);
}

bool createMemBuffCopyHost(cl_context context, size_t size, void* init_data, cl_mem& buf, bool devWriteEna, bool hostReadbackEna) {
	cl_mem_flags flags = CL_MEM_COPY_HOST_PTR
		| (devWriteEna ? CL_MEM_READ_WRITE : CL_MEM_READ_ONLY)
		| (hostReadbackEna ? CL_MEM_HOST_READ_ONLY : CL_MEM_HOST_NO_ACCESS);
	cl_int err;
	buf = clCreateBuffer(context, flags, size, init_data, &err);
	if (err != CL_SUCCESS) {
		printErr("allocating memory buffer", err);
		return false;
	}
	_mem2release.push_back(buf);
	return true;
}

bool createMemBuffFromDevToHost(cl_context context, size_t size, cl_mem& buf, bool devReadbackEna, bool hostWriteEna) {
	cl_mem_flags flags = devReadbackEna ? CL_MEM_READ_WRITE : CL_MEM_WRITE_ONLY;
	if (!hostWriteEna) flags = flags | CL_MEM_HOST_READ_ONLY;
	cl_int err;
	buf = clCreateBuffer(context, flags, size, NULL, &err);
	if (err != CL_SUCCESS) {
		printErr("allocating memory buffer", err);
		return false;
	}
	_mem2release.push_back(buf);
	return true;
}

bool feedData(cl_command_queue queue, const void* data, size_t size, cl_mem buf) {
	cl_int err = clEnqueueWriteBuffer(queue, buf, CL_TRUE, 0, size, data, 0, NULL, NULL);
	if (err) {
		printErr("Filling memory buffer", err);
		return false;
	}
	return true;
}

bool enqueueCopyBuffer(cl_command_queue queue, cl_mem src, cl_mem dest, size_t size, size_t src_offs, size_t dest_offs, vector<cl_event>* evnts2wait) {
	//cl_event new_evnt;
	cl_uint err;
	if (evnts2wait)
		//err = clEnqueueCopyBuffer(queue, src, dest, src_offs, dest_offs, size, evnts2wait->size(), evnts2wait->data(), &new_evnt);
		err = clEnqueueCopyBuffer(queue, src, dest, src_offs, dest_offs, size, evnts2wait->size(), evnts2wait->data(), NULL);
	else
		//err = clEnqueueCopyBuffer(queue, src, dest, src_offs, dest_offs, size, 0, NULL, &new_evnt);
		err = clEnqueueCopyBuffer(queue, src, dest, src_offs, dest_offs, size, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
		printErr("copying memory buffer", err);
		//return NULL;
		return false;
	}
	//return new_evnt;
	return true;
}

bool getKernelWithArgs(cl_program program, const char* name, cl_kernel& kernel, size_t argc, ...) {
	cl_int err;
	kernel = clCreateKernel(program, name, &err);
	if (err != CL_SUCCESS) {
		printErr("selecting Kernel for name", err);
		return false;
	}

	va_list args;
	va_start(args, argc);
	for (size_t n = 0; n < argc; ++n) {
		size_t arg_size = va_arg(args, size_t);
		const void* arg_ptr = va_arg(args, const void*);
		err = clSetKernelArg(kernel, n, arg_size, arg_ptr);
		if (err != CL_SUCCESS) {
			va_end(args);
			printErr("setting Kernel-argument", err);
			return false;
		}
	}
	va_end(args);
	return true;
}

bool enqueueKernel(cl_command_queue queue, cl_kernel kernel,
	size_t dim, const size_t* offset, const size_t* n_cells,
	vector<cl_event> *evnts2wait) {
	//cl_event new_evnt;
	cl_int err;
	if (evnts2wait)
		//err = clEnqueueNDRangeKernel(queue, kernel, dim, offset, n_cells, NULL, evnts2wait->size(), evnts2wait->data(), &new_evnt);
		err = clEnqueueNDRangeKernel(queue, kernel, dim, offset, n_cells, NULL, evnts2wait->size(), evnts2wait->data(), NULL);
	else
		//err = clEnqueueNDRangeKernel(queue, kernel, dim, offset, n_cells, NULL, 0, NULL, &new_evnt);
		err = clEnqueueNDRangeKernel(queue, kernel, dim, offset, n_cells, NULL, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
		printErr("enqueuing kernel", err);
		//return NULL;
		return false;
	}
	//return new_evnt;
	return true;
}

bool getExecutionStatus(cl_event evnt, cl_int& status) {
	cl_int err = clGetEventInfo(evnt, CL_EVENT_COMMAND_EXECUTION_STATUS, sizeof(status), &status, NULL);
	if (err != CL_SUCCESS) {
		printErr("querying execution status", err);
		return false;
	}
	return true;
}

bool readResult(cl_command_queue queue, cl_mem buffer, size_t size, void* data) {
	cl_int err = clEnqueueReadBuffer(queue, buffer, CL_TRUE, 0, size, data, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
		printErr("copying back result from device", err);
		return false;
	}
	return true;
}

bool releaseKernel(cl_kernel &kernel) {
	if (!kernel)
		return true;
	cl_int err = clReleaseKernel(kernel);
	if (err != CL_SUCCESS) {
		printErr("releasing Kernel", err);
		return false;
	}
	kernel = NULL;
	return true;
}

bool releaseMem(cl_mem &buff) {
	if (!buff)
		return 0;
	cl_int err = clReleaseMemObject(buff);
	if (err != CL_SUCCESS) {
		printErr("releasing memory buffer", err);
		return false;
	}
	buff = NULL;
	return true;
}

bool releaseQueue(cl_command_queue &queue) {
	if (!queue)
		return true;
	cl_int err = clReleaseCommandQueue(queue);
	if (err != CL_SUCCESS) {
		printErr("destroying command queue", err);
		return false;
	}
	queue = NULL;
	return true;
}

bool releaseResources() {
	bool success = true;
	for (cl_kernel &kernel : _kernels2release)
		success = releaseKernel(kernel) && success;
	_kernels2release.clear();
	for (cl_mem &memObj : _mem2release)
		success = releaseMem(memObj) && success;
	_mem2release.clear();
	success = releaseQueue(_queue)  && success;
	return success;
}

bool releaseProgram(cl_program &program) {
	if (!program)
		return true;
	cl_int err = clReleaseProgram(program);
	if (err != CL_SUCCESS) {
		printErr("destroying Program object", err);
		return false;
	}
	program = NULL;
	return true;
}

bool releaseContext(cl_context &context) {
	if (!context)
		return true;
	cl_int err = clReleaseContext(context);
	if (err != CL_SUCCESS) {
		printErr("destroying Context", err);
		return false;
	}
	context = NULL;
	return true;
}

bool releaseDevice(cl_device_id &device) {
	if (!device)
		return true;
	cl_int err = clReleaseDevice(device);
	if (err != CL_SUCCESS) {
		printErr("releasing device", err);
		return false;
	}
	device = NULL;
	return true;
}

bool deinitCL() {
	bool success = releaseResources();
	for (cl_program &prog : _progsBuilt)
		success = releaseProgram(prog) && success;
	_progsBuilt.clear();
	success = releaseContext(_context) && success;
	success = releaseDevice(_device) && success;
	return success;
}

void printErr(const char* operation, cl_int err) {
	cerr << "Error when " << operation << ". Code: " << err << endl;
	if (err == CL_INVALID_VALUE) cerr << "\tINVALID VALUE" << endl;  // invalid argument value given to CL API function

	// get device of platform
	if (err == CL_INVALID_PLATFORM) cerr << "\tINVALID PLATFORM" << endl;
	if (err == CL_DEVICE_NOT_FOUND) cerr << "\t" << "DEVICE NOT FOUND" << endl;  // if (count==0) instead

	// create context for devices
	if (err == CL_INVALID_PROPERTY) cerr << "\tINVALID PROPERTY" << endl;
	if (err == CL_INVALID_DEVICE) cerr << "\tINVALID DEVICE" << endl;
	if (err == CL_DEVICE_NOT_AVAILABLE) cerr << "\t" << "tDEVICE NOT AVAILABLE" << endl;

	// create and use command-queue for device
	if (err == CL_INVALID_CONTEXT) cerr << "\tINVALID CONTEXT" << endl;
	if (err == CL_INVALID_QUEUE_PROPERTIES) cerr << "\tINVALID QUEUE PROPERTIES" << endl;
	if (err == CL_INVALID_DEVICE_QUEUE) cerr << "\tINVALID DEVICE QUEUE OBJECT" << endl;  // both copying buffers and enqueuing kernels

	// create and build program
	if (err == CL_INVALID_BINARY) cerr << "\tINVALID BINARY" << endl;  // only clCreateProgramWithBinary()
	if (err == CL_INVALID_PROGRAM) cerr << "\tINVALID PROGRAM OBJECT" << endl;
	if (err == CL_COMPILER_NOT_AVAILABLE) cerr << "\t" << "COMPILER NOT AVAILABLE" << endl;  // clBuildProgram(), clCompileProgram()
	if (err == CL_LINKER_NOT_AVAILABLE) cerr << "\t" << "LINKER NOT AVAILABLE" << endl;  // clLinkProgram()
	if (err == CL_INVALID_BUILD_OPTIONS) cerr << "\tINVALID BUILD OPTIONS" << endl;  // clBuildProgram()
	if (err == CL_INVALID_COMPILER_OPTIONS) cerr << "\tINVALID COMPILER OPTIONS" << endl;  // clCompileProgram()
	if (err == CL_INVALID_LINKER_OPTIONS) cerr << "\tINVALID LINKER OPTIONS" << endl;  // clLinkProgram()
	if (err == CL_BUILD_PROGRAM_FAILURE) cerr << "\t" << "BUILD PROGRAM FAILURE" << endl;  // clBuildProgram()
	if (err == CL_COMPILE_PROGRAM_FAILURE) cerr << "\t" << "COMPILE PROGRAM FAILURE" << endl;  // clCompileProgram()
	if (err == CL_LINK_PROGRAM_FAILURE) cerr << "\t" << "LINK PROGRAM FAILURE" << endl;  // clLinkProgram()

	// memory buffers
	if (err == CL_INVALID_BUFFER_SIZE) cerr << "\tINVALID BUFFER SIZE" << endl;  // creation only
	if (err == CL_INVALID_HOST_PTR) cerr << "\tINVALID HOST PTR" << endl;  // creation only
	if (err == CL_MEM_OBJECT_ALLOCATION_FAILURE) cerr << "\t" << "MEM OBJECT ALLOCATION FAILURE" << endl;  // creation only
	if (err == CL_OUT_OF_HOST_MEMORY) cerr << "\t" << "OUT OF HOST MEMORY" << endl;
	if (err == CL_INVALID_MEM_OBJECT) cerr << "INVALID MEMORY OBJECT" << endl;  // copying only
	if (err == CL_MEM_COPY_OVERLAP) cerr << "\t" << "MEM COPY OVERLAP" << endl;  // copying only
	if (err == CL_OUT_OF_RESOURCES) cerr << "\t" << "OUT OF RESOURCES" << endl;  // copying only

	// get and parametrize kernel of program
	if (err == CL_INVALID_KERNEL_NAME) cerr << "\tKERNEL NAME NOT FOUND" << endl;
	if (err == CL_INVALID_KERNEL_DEFINITION) cerr << "\tKERNEL DEFINITION MISMATCH" << endl;  // when using multiple devices for context
	if (err == CL_INVALID_KERNEL) cerr << "\tINVALID KERNEL OBJECT" << endl;
	if (err == CL_INVALID_ARG_INDEX) cerr << "\tINVALID ARGUMENT INDEX" << endl;
	if (err == CL_INVALID_ARG_SIZE) cerr << "\tINVALID ARGUMENT SIZE" << endl;
	if (err == CL_INVALID_ARG_VALUE) cerr << "\tINVALID ARGUMENT VALUE" << endl;
	//if (err == CL_KERNEL_ARG_INFO_NOT_AVAILABLE) cerr << "\t" << "KERNEL ARG INFO NOT AVAILABLE" << endl;  // querying back

	// enqueue, runtime
	if (err == CL_INVALID_WORK_DIMENSION) cerr << "\tINVALID WORK DIMENSION" << endl;
	if (err == CL_INVALID_GLOBAL_WORK_SIZE) cerr << "\tINVALID GLOBAL WORK SIZE" << endl;
	if (err == CL_INVALID_GLOBAL_OFFSET) cerr << "\tINVALID GLOBAL OFFSET" << endl;
	if (err == CL_INVALID_WORK_GROUP_SIZE) cerr << "\tINVALID WORKGROUP SIZE" << endl;
	if (err == CL_INVALID_WORK_ITEM_SIZE) cerr << "\tINVALID WORKITEM SIZE" << endl;
	if (err == CL_INVALID_KERNEL_ARGS) cerr << "\tKERNEL ARGUMENTS NOT SPECIFIED" << endl;
	if (err == CL_INVALID_PROGRAM_EXECUTABLE) cerr << "\tINVALID PROGRAM EXECUTABLE" << endl;
}