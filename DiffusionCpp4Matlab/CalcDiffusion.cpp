#include <stdlib.h>
#include <ctype.h>
#include "mex.h"
#include "datagrid.h"
#include "methodics.h"
#include "diffusionlib.h"


#define MAX_BC_LEN 10

bool inline mxIsVector(const mxArray *pa);
enum BC { FixedBC, Periodic, Isolated };
bool inline parseBC(const char* sBC, BC &eBC);
bool inline bcToStr(const BC &eBC, char * sBC);
void getDatagrid(const mxArray *mxData, const mxArray *mxRinit, const mxArray *mxRfin, Datagrid &grid);
void mxComposeDatagrid(const Datagrid &grid, mxArray *&mxData, bool rowPreferred);
void mxComposeDatagrid(const Datagrid &grid, mxArray *&mxData, mxArray *&mxRinit, mxArray *&mxRfin, bool rowPreferred);


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	// local variables:
	double coeff = NAN;
	char sBC[MAX_BC_LEN] = "";
	BC eBC;
	size_t Nt = 0;
	double Tinit = NAN, Tfin = NAN;
	Datagrid grid;
	char inputFile[FILENAME_MAX] = "", outputFile[FILENAME_MAX] = "", *fullPath;

	// checking and parsing params:
	if (nrhs < 6 || nrhs > 9)
		mexErrMsgIdAndTxt("CalcDiffusion:WrongNumberOfInput", "CalcDiffusion needs at least 6 and at most 9 arguments!");
	if (nlhs == 2 || nlhs > 3)
		mexErrMsgIdAndTxt("CalcDiffusion:WrongNumberOfOutput", "CalcDiffusion can return 1 or 3 arguments!");
	if (!mxIsDouble(prhs[0]) || !mxIsScalar(prhs[0]))
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "First parameter must be the diffusion coefficient (double scalar)!");
	coeff = *mxGetDoubles(prhs[0]);
	if (!mxIsChar(prhs[1]) || !mxIsVector(prhs[1]))
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "Second parameter must be the name of BC-type (char vector)");
	mxGetString(prhs[1], sBC, MAX_BC_LEN);
	if (parseBC(sBC, eBC))
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamValue", "Invalid BC: %s", sBC);
#ifdef _WIN64
	if (!mxIsUint64(prhs[2]) || !mxIsScalar(prhs[2]))
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "Third parameter must be the number of timesteps (uint64 scalar)!");
	Nt = *mxGetUint64s(prhs[2]);
#else
	if (!mxIsUint32(prhs[2]) || !mxIsScalar(prhs[2]))
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "Third parameter must be the number of timesteps (uint32 scalar)!");
	Nt = *mxGetUint32s(prhs[2]);
#endif
	if (!mxIsDouble(prhs[3]) || !mxIsScalar(prhs[3]))
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "Fourth parameter must be the initial timepoint (double scalar)!");
	Tinit = *mxGetDoubles(prhs[3]);
	if (!mxIsDouble(prhs[4]) || !mxIsScalar(prhs[4]))
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "Fifth parameter must be the final timepoint (double scalar)!");
	Tfin = *mxGetDoubles(prhs[4]);
	if (mxIsDouble(prhs[5])) {
		if (nrhs != 9 && nrhs != 8)
			mexErrMsgIdAndTxt("CalcDiffusion:WrongNumberOfInput", "If 6th parameter is the input data matrix, 2 or 3 more parameters can be accepted!");
		if (!mxIsDouble(prhs[6]) || !mxIsVector(prhs[6]) || !mxIsDouble(prhs[7]) || !mxIsVector(prhs[7]))
			mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "The parameters after the input data matrix must be the initial and final limits of axes (2 pcs of double vector)!");
		if (mxGetNumberOfElements(prhs[6]) != mxGetNumberOfElements(prhs[7]))
			mexErrMsgIdAndTxt("CalcDiffusion:WrongParamSize", "The parameters giving the initial and final limits of axes (parameter #7 and #8) must have the same number of elements!");
		getDatagrid(prhs[5], prhs[6], prhs[7], grid);
	}
	else if (mxIsChar(prhs[5]) && mxIsVector(prhs[5])) {
		if (nrhs != 7 && nrhs != 6)
			mexErrMsgIdAndTxt("CalcDiffusion:WrongNumberOfInput", "If 6th parameter is the input file name, at most 1 more parameters can be accepted!");
		mxGetString(prhs[5], inputFile, FILENAME_MAX);
	}
	else
		mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "6th parameter must be either input data (double matrix) or the name of input file (char vector)!");
	if (nrhs == 9 || nrhs == 7) {
		if (!mxIsChar(prhs[nrhs - 1]) || !mxIsVector(prhs[nrhs - 1]))
			mexErrMsgIdAndTxt("CalcDiffusion:WrongParamTypeOrSize", "The only optional parameter must be the name of output file!");
		mxGetString(prhs[nrhs - 1], outputFile, FILENAME_MAX);
	}
	const void* params[1]{ &coeff };

	// loading file if necessary:
	if (inputFile[0]) {
		mexPrintf("Loading initial data from file: %s...\n", inputFile);
		if (load(inputFile, grid)) {
			fullPath = _fullpath(NULL, inputFile, FILENAME_MAX);
			mexErrMsgIdAndTxt("CalcDiffusion:FileLoadError", "Unable to load input-file: %s\n", fullPath ? fullPath : inputFile);
			free(fullPath);
		}
	}

	// making calculation:
	switch (eBC)
	{
	case BC::FixedBC:
		mexPrintf("Running CNe method with fixed boundary condition, using OpenCL...\n");
		if (euler(grid, (CLParametrizer)CLDiffusion::CNe::parametrizeFixedBC, params, Nt, Tinit, Tfin))
			mexErrMsgIdAndTxt("CalcDiffusion:ComputationError", "Unable to execute OpenCL-based code.");
		mexPrintf("Calculation done.\n");
		break;
	case BC::Periodic:
		mexPrintf("Running Euler's method with periodic boundary condition...\n");
		mexPrintf("Warning: There is no OpenCL-based implementation to this type of boundary condition. Defaulting back to legacy method.\n");
		if (euler(grid.data, Diffusion::Periodic(coeff, grid.scaling), Nt, Tinit, Tfin))
			mexErrMsgIdAndTxt("CalcDiffusion:ComputationError", "Unable to execute native code");
		mexPrintf("Calculation done.\n");
		break;
	case BC::Isolated:
		mexPrintf("Running Euler's method with isolated boundary condition...\n");
		mexPrintf("Warning: There is no OpenCL-based implementation to this type of boundary condition. Defaulting back to legacy method.\n");
		if (euler(grid.data, Diffusion::Isolated(coeff, grid.scaling), Nt, Tinit, Tfin))
			mexErrMsgIdAndTxt("CalcDiffusion:ComputationError", "Unable to execute native code");
		mexPrintf("Calculation done.\n");
		break;
	}

	// writing output file if necessary:
	if (outputFile[0]) {
		fullPath = _fullpath(NULL, outputFile, FILENAME_MAX);
		mexPrintf("Saving result...\n");
		if (save(outputFile, grid))
			mexErrMsgIdAndTxt("CalcDiffusion:FileSaveError", "Unable to save file: %s\n", fullPath ? fullPath : outputFile);
		else
			mexPrintf("Result saved to: %s", fullPath ? fullPath : outputFile);
		free(fullPath);
	}

	// writing output params and releasing grid:
	switch(nlhs) {
	case 0:  // 1st (index-0) param is 'ans'
	case 1:
		mxComposeDatagrid(grid, plhs[0], true);
		break;
	case 3:
		mxComposeDatagrid(grid, plhs[0], plhs[1], plhs[2], true);
		break;
	default:
		mexErrMsgIdAndTxt("CalcDiffusion:WrongNumberOfOutput", "CalcDiffusion can return 1 or 3 arguments!");
	}
	release(grid);
}

bool mxIsVector(const mxArray *pa) {
	return mxGetNumberOfDimensions(pa) == 2 && (mxGetDimensions(pa)[0] == 1 || mxGetDimensions(pa)[1] == 1);
}

void tolower(const char *orig, char *lower) {
	while (*orig)
		*(lower++) = tolower(*(orig++));
	*lower = '\0';
}

bool parseBC(const char* sBC, BC &eBC) {
	char lowerBC[MAX_BC_LEN];
	tolower(sBC, lowerBC);
	if (!strcmp(lowerBC, "fix") || !strcmp(lowerBC, "fixed") || !strcmp(lowerBC, "fixedbc")) {
		eBC = BC::FixedBC;
	}
	else if (!strcmp(lowerBC, "period") || !strcmp(lowerBC, "periodic")) {
		eBC = BC::Periodic;
	}
	else if (!strcmp(lowerBC, "isol") || !strcmp(lowerBC, "isolated")) {
		eBC = BC::Isolated;
	}
	else {
		return true;
	}
	return false;
}

bool bcToStr(const BC &eBC, char *sBC) {
	switch (eBC)
	{
	case BC::FixedBC:
		strcat_s(sBC, MAX_BC_LEN, "FixedBC");
		return false;
	case BC::Periodic:
		strcat_s(sBC, MAX_BC_LEN, "Periodic");
		return false;
	case BC::Isolated:
		strcat_s(sBC, MAX_BC_LEN, "Isolated");
		return false;
	default:
		return true;
	}
}

void getDatagrid(const mxArray *mxData, const mxArray *mxRinit, const mxArray *mxRfin, Datagrid &grid) {
	size_t dim = mxIsScalar(mxData) ? 0 : mxIsVector(mxData) ? 1 : mxGetNumberOfDimensions(mxData);
	//mexPrintf("%d\n", dim);
	// TODO: how about n > 3 ???
	Scaling scaling;
	const size_t *n;
	size_t nn;
	if (dim == 0) {
		n = NULL;
	}
	else if (dim == 1) {
		nn = mxGetNumberOfElements(mxData);
		n = &nn;
	}
	else {
		n = mxGetDimensions(mxData);
	}
	double *r_init = mxGetDoubles(mxRinit);
	double *r_fin = mxGetDoubles(mxRfin);
	if (dim > 0) scaling.X(r_init[0], n[0], r_fin[0]);
	if (dim > 1) scaling.Y(r_init[1], n[1], r_fin[1]);
	if (dim > 2) scaling.Z(r_init[2], n[2], r_fin[2]);
	allocate(grid, scaling);
	memcpy(grid.data, mxGetDoubles(mxData), grid.scaling.nr_tot() * sizeof(double));
}

void mxComposeDatagrid(const Datagrid &grid, mxArray *&mxData, bool rowPreferred) {
	size_t n[] = { grid.scaling.Nx, grid.scaling.Ny, grid.scaling.Nz };
	size_t n_tot = grid.scaling.nr_tot();
	if (grid.scaling.DIM == 1 && rowPreferred)
		mxData = mxCreateDoubleMatrix(1, grid.scaling.Nx, mxComplexity::mxREAL);
	else
		mxData = mxCreateNumericArray(grid.scaling.DIM, n, mxClassID::mxDOUBLE_CLASS, mxComplexity::mxREAL);
	mxDouble *data = (mxDouble*) mxCalloc(n_tot, sizeof(mxDouble));
	memcpy(data, grid.data, n_tot * sizeof(mxDouble));
	mxSetDoubles(mxData, data);
}

void mxComposeDatagrid(const Datagrid &grid, mxArray *&mxData, mxArray *&mxRinit, mxArray *&mxRfin, bool rowPreferred) {
	mxComposeDatagrid(grid, mxData, rowPreferred);
	mxRinit = mxCreateDoubleMatrix(1, grid.scaling.DIM, mxComplexity::mxREAL);
	mxRfin = mxCreateDoubleMatrix(1, grid.scaling.DIM, mxComplexity::mxREAL);
	mxDouble *r_init = (mxDouble*)mxCalloc(grid.scaling.DIM, sizeof(mxDouble));
	mxDouble *r_fin = (mxDouble*)mxCalloc(grid.scaling.DIM, sizeof(mxDouble));
	if (grid.scaling.DIM > 0) { r_init[0] = grid.scaling.Xmin; r_fin[0] = grid.scaling.Xmax; }
	if (grid.scaling.DIM > 1) { r_init[1] = grid.scaling.Ymin; r_fin[1] = grid.scaling.Ymax; }
	if (grid.scaling.DIM > 2) { r_init[2] = grid.scaling.Zmin; r_fin[2] = grid.scaling.Zmax; }
	mxSetDoubles(mxRinit, r_init);
	mxSetDoubles(mxRfin, r_fin);
}
