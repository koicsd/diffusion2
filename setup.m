function SolutionDir = setup()
    [SolutionDir, ~, ~] = fileparts(mfilename('fullpath'));
    addpath(fullfile(SolutionDir, 'matlab'))
    setenv('PATH', [fullfile(SolutionDir, 'bin') pathsep getenv('PATH')])
end