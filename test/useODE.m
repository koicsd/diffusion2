function solution = useODE(Uinit, Rinit, Rfin, Tinit, Tfin, bc, D)
    BCs = struct( ...
        'FixBC', @KerMat_Diffusion_FixedBC, ...
        'Period', @KerMat_Diffusion_Periodic, ...
        'Isol', @KerMat_Diffusion_Isolated ...
    );
    odeopts = odeset('RelTol', 1e-14, 'AbsTol', 1e-14);
    kerfun = BCs.(bc);
    if isvector(Uinit)
        [kernel, precond] = kerfun(Rinit, Rfin, numel(Uinit), D);
        strsol = ode15s(@(t, u) kernel * u, [Tinit, Tfin], (precond * Uinit'), odeopts);
        solution = strsol.y(:,end)';
    else
        dims = size(Uinit);
        [kernel, precond] = kerfun(Rinit, Rfin, dims, D);
        strsol = ode15s(@(t, u) kernel * u, [Tinit, Tfin], (precond * reshape(Uinit, prod(dims), 1)), odeopts);
        solution = reshape(strsol.y(:,end), dims);
    end
end