% Initializing test framework.

GEN_INIT  % First, initialize reference generator

global FILE_NAMES  % path to store initial and result files
FILE_NAMES = struct(...  for communication via CLI, where datafile is necessary
    'Init', "init.grd",...
    'FixBC', "fixedbc.grd",...
    'Period', "periodic.grd",...
    'Isol', "isolated.grd"...
);

global GRID2FILE  % function to store initial data to file
GRID2FILE = @none;
% GRID2FILE = @WriteGrid;

global READBACK  % function to read back numeric result from file
READBACK = @none;
% READBACK = @(filename) ReadGrid(filename, 'row');

global FUNC2TEST  % actual function to be tested
% FUNC2TEST = @runMATLAB;
% FUNC2TEST = @runCLI;
FUNC2TEST = @runMEX;

% Base directory to store CSV files
collector = DataCollector("csv", true);