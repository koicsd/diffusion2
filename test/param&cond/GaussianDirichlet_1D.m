close all
A = 1;  % Norm
mu = 3.4;  % Central value
sigm = 4.2;  % Deviation
U = struct;
U.Init = (A/sigm/sqrt(2*pi))*exp(-((x-mu)/sigm).^2/2);
condscr = convertCharsToStrings(mfilename);
getRefSol = @getNumRefSol;
ctrl_flags = struct('FixBC', true, 'Period', false, 'Isol', false);
COND_READY