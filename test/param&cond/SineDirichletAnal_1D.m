close all
A = 0.8;  % Amplitude
n = 1;  % Order of harmonic (let it be pos. int.)
offs = 1;  % Offset
U = struct;
U.Init = A * sin(n * pi * (x - Xinit) / X) + offs;
condscr = convertCharsToStrings(mfilename);
getRefSol = @(Rinit, Rfin, Uinit, condscr, Tinit, Tfin, bc, D) getAnalSol(A, n, offs, x, Tinit, Tfin, D, bc);
ctrl_flags = struct('FixBC', true, 'Period', false, 'Isol', false);
COND_READY


function Uref = getAnalSol(A, n, offs, x, Tinit, Tfin, D, bc)
    T = Tfin - Tinit;
    Xinit = x(1); Xfin = x(end);
    X = Xfin - Xinit;
    spacial = A * sin(n * pi * (x - Xinit) / X);
    switch bc
        case 'FixBC'
            disp("Using analytic reference solution")
            Uref = exp(-n * n * D * pi * pi * T / X / X) * spacial + offs;
        otherwise
            warning("Unknown analytic solution for '%s', using numeric reference instead...\n", bc)
            Uinit = spacial + offs;
            Uref = getNumRefSol(Xinit, Xfin, Uinit, mfilename, Tinit, Tfin, bc, D);
    end
end
