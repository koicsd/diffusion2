close all

A = 1;  % Amplitude
k = [2.2 0.5];  % Wave number
phi = 1;  % Phase

U = struct;
U.Init = zeros(Ny, Nx);
for i = 1 : Ny
    for j = 1 : Nx
        U.Init(i, j) = A * cos(2 * pi * k * [x(j); y(i)] - phi);
    end
end
condscr = convertCharsToStrings(mfilename);
getRefSol = @getNumRefSol;
ctrl_flags = struct('FixBC', true, 'Period', false, 'Isol', false);
COND_READY
