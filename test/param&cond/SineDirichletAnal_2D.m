close all
A = 0.8;  % Amplitude
n = [1 1];  % Order of harmonic (let it be pos. int.)
offs = 1;  % Offset

U = struct;
U.Init = A * sin(n(1) * pi * (x - Xinit) / X)' * sin(n(2) * pi * (y - Yinit) / Y) + offs;
condscr = convertCharsToStrings(mfilename);
getRefSol = @(Rinit, Rfin, Uinit, smplfunscr, Tinit, Tfin, bc, D) getAnalSol(A, n, offs, x, y, Tinit, Tfin, D, bc);
ctrl_flags = struct('FixBC', true, 'Period', false, 'Isol', false);
COND_READY


function Uref = getAnalSol(A, n, offs, x, y, Tinit, Tfin, D, bc)
    T = Tfin - Tinit;
    [Xinit, Yinit] = deal(x(1), y(1));
    [Xfin, Yfin] = deal(x(end), y(end));
    [X, Y] = deal(Xfin - Xinit, Yfin - Yinit);
    xspacial = sin(n(1) * pi * (x - Xinit) / X);
    yspacial = sin(n(1) * pi * (y - Yinit) / Y);
    spacial = A * xspacial' * yspacial;
    switch bc
        case 'FixBC'
            disp("Using analytic reference solution")
            Uref = exp(-D * T * pi * pi * (n(1) * n(1) / X / X + n(2) * n(2) / Y / Y)) * spacial + offs;
        otherwise
            warning("Unknown analytic solution for '%s', using numeric reference instead...\n", bc)
            Uinit = spacial + offs;
            Uref = getNumRefSol([Xinit, Yinit], [Xfin, Yfin], Uinit, mfilename, Tinit, Tfin, bc, D);
    end
end
