function runCLI(initfile, Nt, Tinit, Tfin, resultfile, bc, D)
    BCs = struct('FixBC', 'fixedbc', 'Period', 'periodic', 'Isol', 'isolated');
    cliCalcDiffusion(D, BCs.(bc), initfile, Tinit, Nt, Tfin, resultfile);
end