function Ufin = runMEX(Uinit, Rinit, Rfin, Nt, Tinit, Tfin, bc, D)
    BCs = struct('FixBC', 'fixedbc', 'Period', 'periodic', 'Isol', 'isolated');
    Ufin = mexCalcDiffusion(D, BCs.(bc), uint64(Nt), Tinit,  Tfin, Uinit, Rinit, Rfin);
end