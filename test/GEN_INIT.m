% Initializing numeric reference factory.

% Directory containing problem generator scripts
generator = Generator("param&cond", true);

global REF_DIR  % directory to cache numeric reference
REF_DIR = "refsol";
if ~isfolder(REF_DIR)
    mkdir(REF_DIR)
end

global CALC_REF  % custom function to calculate numeric reference
CALC_REF = @useODE;