function WriteGrid(filename, data, Rinit,Rfin)
%WRITEDATA Summary of this function goes here
%   Detailed explanation goes here
oneDim = isvector(data);
if oneDim
    Nr = numel(data);
else
    Nr = size(data);
end
DIM = numel(Nr);
N = prod(Nr);
if ~isvector(Rinit) || ~isvector(Rfin)
    error('Rinit and Rfin must be a vector (or a scalar for 1D data)!')
end
if isscalar(Rinit) ~= oneDim || isscalar(Rfin) ~= oneDim
    error('Rinit and Rfin must be a scalar if and only if data is a vector (1D domain)!')
end
if ~oneDim
    if ~isequal(size(Rinit), size(Rfin))
        error('Rinit and Rfin must have the same array-size!')
    end
    if ~isequal(numel(Rinit), DIM) || ~isequal(numel(Rfin), DIM)
        error('Rinit and Rfin must have as many elements as many dimensions the datagrid has!')
    end
end
SIZE_DATA
[fid, errmsg] = fopen(filename, 'w');
if fid == -1
    error('Unable to open file for writting: %s\nReason: %s', absolutepath(filename), errmsg)
end
cleanupObj = onCleanup(@() fclose(fid));
if fwrite(fid, 'KD', 'char') < 2 ...
        || fwrite(fid, DIM, size_t) < 1 ...
        || fwrite(fid, Nr, size_t) < DIM ...
        || fwrite(fid, Rinit, 'double') < DIM ...
        || fwrite(fid, Rfin, 'double') < DIM ...
        || fwrite(fid, data, 'double') < N
    error(['Error writing to file: ' fopen(fid)])
end
end
