function [data, Rinit, Rfin] = ReadGrid(filename, preference)
%READDATA Summary of this function goes here
%   Detailed explanation goes here
SIZE_DATA
[fid, errmsg] = fopen(filename, 'r');
if fid == -1
    error('Unable to open file for reading: %s\nReason: %s', absolutepath(filename), errmsg)
end
cleanupObj = onCleanup(@() fclose(fid));
if (fseek(fid, 0, 'eof') ~= 0)
    error(['Error seaking file: ' fopen(fid)])
end
filelen = ftell(fid);
if (filelen < 2 * size_of_char + 2 * size_of_size + 2 * size_of_double)
    error(['File too short: ' fopen(fid)])
end
if (fseek(fid, 0, 'bof') ~= 0)
    error(['Error seaking file: ' fopen(fid)])
end
bom = fread(fid, 2, 'char=>char')';
if ~isequal(bom, 'KD')
    error(['Wrong BOM in file: ' fopen(fid)])
end
DIM = fread(fid, 1, [size_t '=>double']);
if DIM > 3
    error(['Only up to 3D datagrid supported!\nfile: ' fopen(fid)])
end
Nr = fread(fid, DIM, [size_t '=>double'])';
N = prod(Nr);
Rinit = fread(fid, DIM, 'double=>double')';
Rfin = fread(fid, DIM, 'double=>double')';
if (filelen - ftell(fid)) < N * size_of_double
    error(['Unexpected end of file: ' fopen(fid)])
end
if DIM == 1 && nargin >= 2 && strcmp(preference, 'col')
    data = fread(fid, [Nr 1], 'double=>double');
elseif DIM == 1 && nargin >= 2 && strcmp(preference, 'row')
    data = fread(fid, [1 Nr], 'double=>double');
else
    data = fread(fid, Nr, 'double=>double');
end
if ftell(fid) < filelen
    error(['Unexpected data at the end of file: ' fopen(fid)])
end
end
