if endsWith(mexext, '64')
    size_t = 'uint64';
    size_of_size = 8;
elseif endswith(mexext, '32')
    size_t = 'uint32';
    size_of_size = 4;
end
size_of_double = 8;
size_of_char = 1;