function [kernel, precond] = KerMat_Diffusion_FixedBC(Rinit, Rfin, Nr, coeff)
%[kernel, precond] = KerMat_Diffusion_FixedBC(Rinit, Rfin, Nr, coeff)
%
%   A function to create sparse-matrices for differential equation
%
%       ddata/dt = coeff * Laplace{data}
%
%   to be solved by EulerMethod.m
%   with condition of fixed boundary.
%   To use this, specify a handle to this function
%   as parameter kerfun of EulerMethod.
%
%   Fixed boundary means the derivative ddata/dt or Laplace{data}
%   is considered to be zero at the boundaries
%   (ie. in 1-dimension, the on-boundary derivatives are:
%       ddata/dt(t,x=0) = coeff * (d/dx)^2{data}(t,x=0) = 0
%       ddata/dt(t,x=Xfin) = coeff * (d/dx)^2{data}(t,x=Xfin) = 0
%   where Xfin is the upper limit of the spacious interval)
%
%   Currently, only 1 and 2 spacious dimensions are supported.
%
%   Paremeters:
%       coeff:  the coefficient of the equation.
%               ie. heat-transfer or diffusion coeffitient
%
%       Rinit:  a vector containing the lower limits of spacious axes.
%               numel(Rinit) has to be equal to
%               the number of spacius dimensions;
%               in case of a 1-dimensional problem
%               Rinit must be a scalar
%
%       Rfin:   a vector containing the upper limits of spacious axes.
%               numel(Rfin) has to be equal to
%               the number of spacius dimensions;
%               in case of a 1-dimensional problem
%               Rfin must be a scalar
%
%       Nr:     a vector containing the number of datapoints per axe.
%               numel(Nr) has to be equal to
%               the number of spacius dimensions;
%               in case of a 1-dimensional problem
%               Nr must be a scalar
%
%       Parameters Rinit, Rfin and Nr has to be of the same size:
%               size(Nr) == size(Rinit)
%               size(Nr) == size(Rfin)
%
%   Return values:
%       kernel:     a sparse-matrix to be used to calculate derivatives
%                   in each iteration.
%                   size(kernel) == [prod(Nr), prod(Nr)]
%
%       precond:    a sparse-matrix to be used to precondition data
%                   before first iteration.
%                   size(precond) == [prod(Nr), prod(Nr)]

if ~isvector(Rinit) || ~ isvector(Rfin) || ~isvector(Nr)
    error('Rinit, Rfin and Nr must be a vector (or a scalar for 1D calc.)!')
end
if ~isequal(size(Rinit), size(Nr)) || ~isequal(size(Rfin), size(Nr))
    error('Rinit, Rfin and Nr must have the same array-size!')
end

dim = numel(Nr);
dr = (Rfin - Rinit) ./ (Nr - 1);

switch dim
    case 1
        Nx = Nr;
        dx = dr;
        kernel = coeff * spdiags([  ones(1,Nx-2)    0                   0;
                                    0               repmat(-2, 1,Nx-2)  0
                                    0               0                   ones(1,Nx-2)    ]', [-1 0 1], Nx, Nx) / dx / dx;
        precond = speye(Nx);
        
    case 2
        Ny = Nr(1);
        Nx = Nr(2);
        Yinit = Rinit(1);
        Xinit = Rinit(2);
        Yfin = Rfin(1);
        Xfin = Rfin(2);
        % dy = dr(1);
        % dx = dr(2);
        [kernelY, precondY] = KerMat_Diffusion_FixedBC(Yinit, Yfin, Ny, coeff);
        [kernelX, precondX] = KerMat_Diffusion_FixedBC(Xinit, Xfin, Nx, coeff);
        kernel = kron(kernelX, spdiags([0; ones(Ny-2,1); 0], 0, Ny, Ny)) ...
               + kron(spdiags([0; ones(Nx-2,1); 0], 0, Nx, Nx), kernelY);
        precond = kron(precondX, precondY);
        
    otherwise
        error('Unsupported number of dimensions!')
end
end
