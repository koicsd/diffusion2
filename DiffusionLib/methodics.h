#pragma once

#ifdef DIFFUSIONLIB_EXPORTS
#define DIFFUSIONLIB_API __declspec(dllexport)
#else
#define DIFFUSIONLIB_API __declspec(dllimport)
#endif

#include "datagrid.h"
#include <math.h>
#include <vector>
#include <cl/opencl.h>
using std::vector;


class DIFFUSIONLIB_API Stepper {
protected:
	Scaling _scaling;
	inline Stepper(const Scaling &scaling) : _scaling(scaling) {}
	inline Stepper(const Stepper &other) : _scaling(other._scaling) {}

public:
	const Scaling &scaling = _scaling;
	virtual void precond(const double *raw, double *prec) const = 0;
	virtual void operator()(const double *before, double dt, double *after) const = 0;
	inline Stepper& operator=(const Stepper &rhs) { _scaling = rhs._scaling; return *this; }
};

DIFFUSIONLIB_API bool euler(double *&data, const Stepper &stepper, size_t nt, double t_init, double t_fin);


struct KernelWithRange {
	cl_kernel kernel = NULL;
	size_t dim = 0;
	size_t offs[3] = { 0, 0, 0 };
	size_t size[3] = { 0, 0, 0 };
};

typedef bool(*CLParametrizer)(const void* params[], const Scaling& scaling, double dt,
	cl_context context, cl_mem raw, cl_mem prec,
	vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven);

DIFFUSIONLIB_API bool euler(Datagrid& grid, CLParametrizer parametrizer, const void* params[], size_t nt, double t_init, double t_fin);


struct GridIterator {
	const Scaling &scaling;
	const size_t pageSize, rowSize;
	double *startOfGrid, *endOfGrid;
	double *startOfPage, *endOfPage;
	double *startOfRow, *endOfRow;
	double *elem;

	inline GridIterator(Datagrid& grid, size_t i = 0, size_t j = 0, size_t k = 0) : GridIterator(grid.scaling, grid.data, i, j, k) {}

	inline GridIterator(const Scaling &scaling, double *data, size_t i = 0, size_t j = 0, size_t k = 0) :
		scaling(scaling), rowSize(scaling.Nx), pageSize(scaling.Nx * scaling.Ny) {
		startOfGrid = data;
		endOfGrid = data + scaling.nr_tot();
		jumpPage(k);
		jumpRow(j);
		jumpElem(i);
	}

	inline GridIterator& jumpElem(size_t i) {
		elem = &scaling.lookup(startOfRow, i);
		return *this;
	}

	inline GridIterator& jumpRow(size_t j) {
		startOfRow = scaling.lookupRow(startOfPage, j);
		endOfRow = startOfRow + rowSize;
		return *this;
	}

	inline GridIterator& jumpPage(size_t k) {
		startOfPage = scaling.lookupPage(startOfGrid, k);
		endOfPage = startOfPage + pageSize;
		return *this;
	}

	inline GridIterator& step(ptrdiff_t i = 1) {
		elem += i;
		return *this;
	}

	inline GridIterator& stepRow(ptrdiff_t j = 1) {
		ptrdiff_t i = rowSize * j;
		startOfRow += i;
		endOfRow += i;
		elem += i;
		return *this;
	}

	inline GridIterator& stepPage(ptrdiff_t k = 1) {
		ptrdiff_t i = pageSize * k;
		startOfPage += i;
		endOfPage += i;
		startOfRow += i;
		endOfRow += i;
		elem += i;
		return *this;
	}
};


struct NeighbourIterator {
	const Scaling &scaling;
	const size_t pageSize, rowSize;
	const double *start, *end;
	const double *page, *pageAbove, *pageBelow;
	const double *row, *rowUp, *rowDown, *rowAbove, *rowBelow;
	const double *elem, *left, *right, *up, *down, *above, *below;

	inline NeighbourIterator(Datagrid grid, size_t i = 0, size_t j = 0, size_t k = 0) : NeighbourIterator(grid.scaling, grid.data, i, j, k) {}

	inline NeighbourIterator(const Scaling &scaling, const double *data, size_t i = 0, size_t j = 0, size_t k = 0) :
		scaling(scaling), rowSize(scaling.Nx), pageSize(scaling.Nx * scaling.Ny) {
		start = data;
		end = data + scaling.nr_tot();
		jumpPage(k);
		jumpRow(j);
		jumpElem(i);
	}

	inline NeighbourIterator& jumpElem(size_t i) {
		elem = &scaling.lookup(row, i);
		above = elem - pageSize;
		below = elem + pageSize;
		up = elem - rowSize;
		down = elem + rowSize;
		left = elem - 1;
		right = elem + 1;
		return *this;
	}

	inline NeighbourIterator& jumpRow(size_t j) {
		row = scaling.lookupRow(page, j);
		rowAbove = row - pageSize;
		rowBelow = row + pageSize;
		rowUp = row - rowSize;
		rowDown = row + rowSize;
		return *this;
	}

	inline NeighbourIterator& jumpPage(size_t k) {
		page = scaling.lookupPage(start, k);
		pageAbove = page - pageSize;
		pageBelow = page + pageSize;
		return *this;
	}

	inline NeighbourIterator& step(ptrdiff_t i = 1) {
		elem += i;
		above += i;
		below += i;
		up += i;
		down += i;
		left += i;
		right += i;
		return *this;
	}

	inline NeighbourIterator& stepRow(ptrdiff_t j = 1) {
		ptrdiff_t i = rowSize * j;
		row += i;
		rowAbove += i;
		rowBelow += i;
		rowUp += i;
		rowDown += i;
		elem += i;
		above += i;
		below += i;
		up += i;
		down += i;
		left += i;
		right += i;
		return *this;
	}

	inline NeighbourIterator& stepPage(ptrdiff_t k = 1) {
		ptrdiff_t i = pageSize * k;
		page += i;
		pageAbove += i;
		pageBelow += i;
		row += i;
		rowAbove += i;
		rowBelow += i;
		rowUp += i;
		rowDown += i;
		elem += i;
		above += i;
		below += i;
		up += i;
		down += i;
		left += i;
		right += i;
		return *this;
	}
};

