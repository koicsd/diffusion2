// DiffusionLib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "diffusionlib.h"
#include "CLHelper.h"
#include <math.h>
using std::cerr;
using std::endl;


void Diffusion::_config() {
	if (scaling.DIM >= 1) {
		_Cx = D / pow(scaling.dx(), 2);
		if (scaling.DIM >= 2) {
			_Cy = D / pow(scaling.dy(), 2);
			if (scaling.DIM >= 3) {
				_Cz = D / pow(scaling.dz(), 2);
			}
		}
	}
}

double Diffusion::_step_1D(const double &it, const double &dt, const double &left, const double &right) const {
	return D * (
		(left - 2 * it + right) * _Cx
	) * dt + it;
}

double Diffusion::_step_2D(const double &it, const double &dt,
	const double &left, const double &right, const double &up, const double &down) const {
	return D * (
		(left - 2 * it + right) * _Cx +
		(up - 2 * it + down) * _Cy
	) * dt + it;
}

double Diffusion::_step_3D(const double &it, const double &dt,
	const double &left, const double &right, const double &up, const double &down, const double &above, const double &below) const {
	return D * (
		(left - 2 * it + right) * _Cx +
		(up - 2 * it + down) * _Cy +
		(above - 2 * it + below) * _Cz
	) * dt + it;
}


void Diffusion::FixedBC::precond(const double *raw, double *prec) const {
	memcpy(prec, raw, scaling.nr_tot() * sizeof(double));
}

void Diffusion::FixedBC::operator()(const double *before, double dt, double *after) const {
	NeighbourIterator iter_before(scaling, before);
	GridIterator iter_after(scaling, after);
	switch (scaling.DIM) {
	case 1:
		for (iter_before.jumpElem(1), iter_after.jumpElem(1); iter_before.right < iter_before.end; iter_before.step(), iter_after.step()) {
			*iter_after.elem = _step_1D( *iter_before.elem, dt, *iter_before.left, *iter_before.right);
		}
		break;
	case 2:
		for (iter_before.jumpRow(1), iter_after.jumpRow(1); iter_before.rowDown < iter_before.end; iter_before.stepRow(), iter_after.stepRow()) {
			for (iter_before.jumpElem(1), iter_after.jumpElem(1); iter_before.right < iter_before.rowDown; iter_before.step(), iter_after.step()) {
				*iter_after.elem = _step_2D(*iter_before.elem, dt,
					*iter_before.left, *iter_before.right,
					*iter_before.up, *iter_before.down);
			}
		}
		break;
	case 3:
		for (iter_before.jumpPage(1), iter_after.jumpPage(1); iter_before.pageBelow < iter_before.end; iter_before.stepPage(), iter_after.stepPage()) {
			for (iter_before.jumpRow(1), iter_after.jumpRow(1); iter_before.rowDown < iter_before.pageBelow; iter_before.stepRow(), iter_after.stepRow()) {
				for (iter_before.jumpElem(1), iter_after.jumpElem(1); iter_before.right < iter_before.rowDown; iter_before.step(), iter_after.step()) {
					*iter_after.elem = _step_3D(*iter_before.elem, dt,
						*iter_before.left, *iter_before.right,
						*iter_before.up, *iter_before.down,
						*iter_before.above, *iter_before.below);
				}
			}
		}
		break;
	}
}


void Diffusion::Periodic::precond(const double *raw, double *prec) const {
	memcpy(prec, raw, scaling.nr_tot() * sizeof(double));
	switch (scaling.DIM) {
	case 1:
		scaling.lookup(prec, 0) =
		scaling.lookup(prec, scaling.Nx - 1) =
			(
				scaling.lookup(raw, 0)
				+ scaling.lookup(raw, scaling.Nx - 1)
			) / 2;
		break;
	case 2:
		scaling.lookup(prec, 0, 0) =
		scaling.lookup(prec, scaling.Nx - 1, 0) =
		scaling.lookup(prec, 0, scaling.Ny - 1) =
		scaling.lookup(prec, scaling.Nx - 1, scaling.Ny - 1) =
			(
				scaling.lookup(raw, 0, 0)
				+ scaling.lookup(raw, scaling.Nx - 1, 0)
				+ scaling.lookup(raw, 0, scaling.Ny - 1)
				+ scaling.lookup(raw, scaling.Nx - 1, scaling.Ny - 1)
			) / 4;
		for (size_t i = 1; i < scaling.Nx - 1; ++i) {
			scaling.lookup(prec, i, 0) =
			scaling.lookup(prec, i, scaling.Ny - 1) =
				(
					scaling.lookup(raw, i, 0)
					+ scaling.lookup(raw, i, scaling.Ny - 1)
				) / 2;
		}
		for (size_t j = 1; j < scaling.Ny - 1; ++j) {
			scaling.lookup(prec, 0, j) =
			scaling.lookup(prec, scaling.Nx - 1, j) =
				(
					scaling.lookup(raw, 0, j)
					+ scaling.lookup(raw, scaling.Nx - 1, j)
				) / 2;
		}
		break;
	case 3:
		/*scaling.lookup(prec, 0, 0, 0) =
		scaling.lookup(prec, scaling.Nx - 1, 0) =
		scaling.lookup(prec, 0, scaling.Ny - 1, 0) =
		scaling.lookup(prec, scaling.Nx - 1, scaling.Ny - 1, 0) =
		scaling.lookup(prec, 0, 0, scaling.Nz - 1) =
		scaling.lookup(prec, scaling.Nx - 1, scaling.Nz - 1) =
		scaling.lookup(prec, 0, scaling.Ny - 1, scaling.Nz - 1) =
		scaling.lookup(prec, scaling.Nx - 1, scaling.Ny - 1, scaling.Nz - 1) =
			(
				scaling.lookup(raw, 0, 0, 0)
				+ scaling.lookup(raw, scaling.Nx - 1, 0, 0)
				+ scaling.lookup(raw, 0, scaling.Ny - 1, 0)
				+ scaling.lookup(raw, scaling.Nx - 1, scaling.Ny - 1, 0)
				+ scaling.lookup(raw, 0, 0, scaling.Nz - 1)
				+ scaling.lookup(raw, scaling.Nx - 1, 0, scaling.Nz - 1)
				+ scaling.lookup(raw, 0, scaling.Ny - 1, scaling.Nz - 1)
				+ scaling.lookup(raw, scaling.Nx - 1, scaling.Ny - 1, scaling.Nz - 1)
			) / 8;
		for (size_t j = 1; j < scaling.Ny - 1; ++j) {
			scaling.lookup(prec, 0, j, 0) =
			scaling.lookup(prec, scaling.Nx - 1, j, 0) =
			scaling.lookup(prec, 0, j, scaling.Nz - 1) =
			scaling.lookup(prec, scaling.Nx - 1, j, scaling.Nz - 1) =
				(
					scaling.lookup(raw, 0, j, 0)
					+ scaling.lookup(raw, scaling.Nx - 1, j, 0)
					+ scaling.lookup(raw, 0, j, scaling.Nz - 1)
					+ scaling.lookup(raw, scaling.Nx - 1, j, scaling.Nz - 1)
				) / 4;
			for (size_t i = 1; i < scaling.Nx - 1; ++i) {	
				scaling.lookup(prec, i, j, 0) =
				scaling.lookup(prec, i, j, scaling.Nz - 1) =
					(
						scaling.lookup(raw, i, j, 0)
						+ scaling.lookup(raw, i, j, scaling.Nz - 1)
					) / 2;
			}
		}
		for (size_t k = 1; k < scaling.Ny - 1; ++k) {
			scaling.lookup(prec, 0, 0, k) =
			scaling.lookup(prec, scaling.Nx - 1, 0, k) =
			scaling.lookup(prec, scaling.Nx - 1, scaling.Ny - 1, k) =
			scaling.lookup(prec, scaling.Nx - 1, scaling.Ny - 1, k) =
				(
					scaling.lookup(raw, 0, 0, k)
					+ scaling.lookup(raw, scaling.Nx - 1, 0, k)
					+ scaling.lookup(raw, 0, scaling.Ny - 1, k)
					+ scaling.lookup(raw, scaling.Nx - 1, scaling.Ny - 1, k)
				) / 4;
			for (size_t j = 1; j < scaling.Nx - 1; ++j) {
				scaling.lookup(prec, 0, j, k) =
				scaling.lookup(prec, scaling.Nx - 1, j, k) =
					(
						scaling.lookup(raw, 0, j, k)
						+ scaling.lookup(raw, scaling.Nx - 1, j, k)
					) / 2;
			}
		}
		for (size_t i = 1; i < scaling.Ny - 1; ++i) {
			scaling.lookup(prec, i, 0, 0) =
			scaling.lookup(prec, i, scaling.Ny - 1, 0) =
			scaling.lookup(prec, i, 0, scaling.Nz - 1) =
			scaling.lookup(prec, i, scaling.Ny - 1, scaling.Nz - 1) =
				(
					scaling.lookup(raw, i, 0, 0)
					+ scaling.lookup(raw, i, scaling.Ny - 1, 0)
					+ scaling.lookup(raw, i, 0, scaling.Nz - 1)
					+ scaling.lookup(raw, i, scaling.Ny - 1, scaling.Nz - 1)
				) / 4;
			for (size_t k = 1; k < scaling.Nx - 1; ++k) {
				scaling.lookup(prec, i, 0, k) =
				scaling.lookup(prec, i, scaling.Ny - 1, k) =
					(
						scaling.lookup(raw, i, 0, k)
						+ scaling.lookup(raw, i, scaling.Ny - 1, k)
					) / 2;
			}
		}*/
		break;
	}
}

void Diffusion::Periodic::operator()(const double *before, double dt, double *after) const {
	//memcpy(after, before, scaling.nr_tot() * sizeof(double));
	NeighbourIterator iter_before(scaling, before);
	GridIterator iter_after(scaling, after);
	//int i, j, k;
	switch (scaling.DIM) {
	case 1:
		*iter_after.elem = _step_1D(*iter_before.elem, dt, *(iter_before.end - 2), *iter_before.right);  // first element
		for (iter_before.step(), iter_after.step(); iter_before.right < iter_before.end; iter_before.step(), iter_after.step()) {  // inner elements
			*iter_after.elem = _step_1D(*iter_before.elem, dt, *iter_before.left, *iter_before.right);
		}
		*iter_after.elem /*iter_after.end - 1*/ = *iter_after.startOfGrid;  // last element
		break;
	case 2: {
			NeighbourIterator iter_before2(iter_before);  // 2nd iterators for last row for parallel iteration
			GridIterator iter_after2(iter_after);
			iter_before2.jumpRow(scaling.Ny - 1).jumpElem(0);
			iter_after2.jumpRow(scaling.Ny - 1).jumpElem(0);
			*iter_after.elem = _step_2D(*iter_before.elem, dt,  // top left
				*(iter_before.rowDown - 2), *iter_before.right,
				*iter_before2.up, *iter_before.down);
			*iter_after2.elem = *iter_after.elem;  // bottom left
			for (/*i = 1, */iter_before.step(), iter_before2.step(), iter_after.step(), iter_after2.step();  // first and last row
					iter_before.right < iter_before.rowDown;
					iter_before.step(), iter_before2.step(), iter_after.step(), iter_after2.step()/*, ++i*/) {
				*iter_after.elem = _step_2D(*iter_before.elem, dt,  // element of first row
					*iter_before.left, *iter_before.right,
					*iter_before2.up, *iter_before.down);
				*iter_after2.elem = *iter_after.elem;  // element of last row
			}
			*iter_after2.elem = *iter_after.elem = *iter_after.startOfRow;  // top right, bottom right
		}
			
		for (/*j = 1, */iter_before.stepRow(), iter_after.stepRow(); iter_before.rowDown < iter_before.end; iter_before.stepRow(), iter_after.stepRow()/*, ++j*/) {  // inner rows
			iter_before.jumpElem(0), iter_after.jumpElem(0);
			*iter_after.elem = _step_2D(*iter_before.elem, dt,  // start of row
				*(iter_before.rowDown - 2), *iter_before.right,
				*iter_before.up, *iter_before.down);
			for (/*i = 1, */iter_before.step(), iter_after.step(); iter_before.right < iter_before.rowDown; iter_before.step(), iter_after.step()/*, ++i*/) {  // inner elements of row
				*iter_after.elem = _step_2D(*iter_before.elem, dt,
					*iter_before.left, *iter_before.right,
					*iter_before.up, *iter_before.down);
			}
			*iter_after.elem = *iter_after.startOfRow;  // end of row
		}
			break;
	case 3:
		/*scaling.lookup(after, 0, 0, 0) =
		scaling.lookup(after, scaling.Nx - 1, 0, 0) =
		scaling.lookup(after, 0, scaling.Ny - 1, 0) =
		scaling.lookup(after, scaling.Nx - 1, scaling.Ny - 1, 0) =
		scaling.lookup(after, 0, 0, scaling.Ny - 1) =
		scaling.lookup(after, scaling.Nx - 1, 0, scaling.Nz - 1) =
		scaling.lookup(after, 0, scaling.Ny - 1, scaling.Nz - 1) =
		scaling.lookup(after, scaling.Nx - 1, scaling.Ny - 1, scaling.Nz - 1) += D * (
			(scaling.lookup(before, scaling.Nx - 2, 0, 0) - 2 * scaling.lookup(before, 0, 0, 0) + scaling.lookup(before, 1, 0, 0)) * _Cx +
				(scaling.lookup(before, 0, scaling.Ny - 2, 0) - 2 * scaling.lookup(before, 0, 0, 0) + scaling.lookup(before, 0, 1, 0)) * _Cy +
				(scaling.lookup(before, 0, 0, scaling.Nz - 1) - 2 * scaling.lookup(before, 0, 0, 0) + scaling.lookup(before, 0, 0, 1)) * _Cz
			) * dt;
		for (size_t i = 1; i < scaling.Nx; ++i) {
			scaling.lookup(after, i, 0, 0) =
			scaling.lookup(after, i, scaling.Ny - 1, 0) =
			scaling.lookup(after, i, 0, scaling.Nz - 1) =
			scaling.lookup(after, i, scaling.Ny - 1, scaling.Nz - 1) += D * (
				(scaling.lookup(before, i - 1, 0, 0) - 2 * scaling.lookup(before, i, 0, 0) + scaling.lookup(before, i + 1, 0, 0)) * _Cx +
				(scaling.lookup(before, i, scaling.Ny - 2, 0) - 2 * scaling.lookup(before, i, 0, 0) + scaling.lookup(before, i, 1, 0)) * _Cy +
				(scaling.lookup(before, i, 0, scaling.Nz - 2) - 2 * scaling.lookup(before, i, 0, 0) + scaling.lookup(before, i, 0, 1)) * _Cz
			) * dt;
		}
		for (size_t j = 1; j < scaling.Ny - 1; ++j) {
			scaling.lookup(after, 0, j, 0) =
			scaling.lookup(after, scaling.Nx - 1, j, 0) =
			scaling.lookup(after, 0, j, scaling.Nz - 1) =
			scaling.lookup(after, scaling.Nx - 1, j, scaling.Nz - 1) += D * (
				(scaling.lookup(before, scaling.Nx - 2, j, 0) - 2 * scaling.lookup(before, 0, j, 0) + scaling.lookup(before, 1, j, 0)) * _Cx +
				(scaling.lookup(before, 0, j - 1, 0) - 2 * scaling.lookup(before, 0, j, 0) + scaling.lookup(before, 0, j + 1, 0)) * _Cy +
				(scaling.lookup(before, 0, j, scaling.Nz - 2) - 2 * scaling.lookup(before, 0, j, 0) + scaling.lookup(before, 0, j, 1)) * _Cz
			) * dt;
			for (size_t i = 1; i < scaling.Nx - 1; ++i) {
				scaling.lookup(after, i, j, 0) =
				scaling.lookup(after, i, j, scaling.Nz - 1) += D * (
					(scaling.lookup(before, i - 1, j, 0) - 2 * scaling.lookup(before, i, j, 0) + scaling.lookup(before, i + 1, j, 0)) * _Cx +
					(scaling.lookup(before, i, j - 1, 0) - 2 * scaling.lookup(before, i, j, 0) + scaling.lookup(before, i, j + 1, 0)) * _Cy +
					(scaling.lookup(before, i, j, scaling.Nz - 2) - 2 * scaling.lookup(before, i, j, 0) + scaling.lookup(before, i, j, 1)) * _Cz
				) * dt;
			}
		}
		for (size_t k = 1; k < scaling.Nz - 1; ++k) {
			scaling.lookup(after, 0, 0, k) =
			scaling.lookup(after, scaling.Nx - 1, 0, k) =
			scaling.lookup(after, 0, scaling.Ny - 1, k) =
			scaling.lookup(after, scaling.Nx - 1, scaling.Ny - 1, k) += D * (
				(scaling.lookup(before, scaling.Nx - 2, 0, k) - 2 * scaling.lookup(before, 0, 0, k) + scaling.lookup(before, 1, 0, k)) * _Cx +
				(scaling.lookup(before, 0, scaling.Ny - 2, k) - 2 * scaling.lookup(before, 0, 0, k) + scaling.lookup(before, 0, 1, k)) * _Cy +
				(scaling.lookup(before, 0, 0, k - 1) - 2 * scaling.lookup(before, 0, 0, k) + scaling.lookup(before, 0, 0, k + 1)) * _Cz
			) * dt;
			for (size_t i = 1; i < scaling.Nx - 1; ++i) {
				scaling.lookup(after, i, 0, k) =
				scaling.lookup(after, i, scaling.Ny - 1, k) += D * (
					(scaling.lookup(before, i - 1, 0, k) - 2 * scaling.lookup(before, i, 0, k) + scaling.lookup(before, i + 1, 0, k)) * _Cx +
					(scaling.lookup(before, i, scaling.Ny - 2, k) - 2 * scaling.lookup(before, i, 0, k) + scaling.lookup(before, i, 1, k)) * _Cy +
					(scaling.lookup(before, i, 0, k - 1) - 2 * scaling.lookup(before, i, 0, k) + scaling.lookup(before, i, 0, k + 1)) * _Cz
				) * dt;
			}
			for (size_t j = 1; j < scaling.Ny - 1; ++j) {
				scaling.lookup(after, 0, j, k) =
				scaling.lookup(after, scaling.Nx - 1, j, k) += D * (
					(scaling.lookup(before, scaling.Nx - 2, j, k) - 2 * scaling.lookup(before, 0, j, k) + scaling.lookup(before, 1, j, k)) * _Cx +
					(scaling.lookup(before, 0, j - 1, k) - 2 * scaling.lookup(before, 0, j, k) + scaling.lookup(before, 0, j + 1, k)) * _Cy +
					(scaling.lookup(before, 0, j, k - 1) - 2 * scaling.lookup(before, 0, j, k) + scaling.lookup(before, 0, j, k + 1)) * _Cz
				) * dt;
				for (size_t i = 1; i < scaling.Nx - 1; ++i) {
					scaling.lookup(after, i, j, k) += D * (
						(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
						(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
						(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
					) * dt;
				}
			}
		}*/
		break;
	}
}


void Diffusion::Isolated::precond(const double *raw, double *prec) const {
	memcpy(prec, raw, scaling.nr_tot() * sizeof(double));
}

void Diffusion::Isolated::operator()(const double *before, double dt, double *after) const {
	//memcpy(after, before, scaling.nr_tot() * sizeof(double));
	NeighbourIterator iter_before(scaling, before);
	GridIterator iter_after(scaling, after);
	//int i, j, k;
	switch (scaling.DIM) {
	case 1:
		*iter_after.elem = _step_1D(*iter_before.elem, dt, *iter_before.elem, *iter_before.right);  // first element
		for (iter_before.step(), iter_after.step(); iter_before.right < iter_before.end; iter_before.step(), iter_after.step()) {  // inner elements
			*iter_after.elem = _step_1D(*iter_before.elem, dt, *iter_before.left, *iter_before.right);
		}
		*iter_after.elem = _step_1D(*iter_before.elem, dt, *iter_before.left, *iter_before.elem);  // last element
		break;
	case 2: {
			NeighbourIterator iter_before2(iter_before);  // 2nd iterators for last row for parallel iteration
			GridIterator iter_after2(iter_after);
			iter_before2.jumpRow(scaling.Ny - 1).jumpElem(0);
			iter_after2.jumpRow(scaling.Ny - 1).jumpElem(0);
			*iter_after.elem = _step_2D(*iter_before.elem, dt,  // top left
				*iter_before.elem, *iter_before.right,
				*iter_before.elem, *iter_before.down);
			*iter_after2.elem = _step_2D(*iter_before2.elem, dt,  // bottom left
				*iter_before2.elem, *iter_before2.right,
				*iter_before2.up, *iter_before2.elem);
			for (/*i = 1, */iter_before.step(), iter_before2.step(), iter_after.step(), iter_after2.step();  // first and last row
					iter_before.right < iter_before.rowDown;
					iter_before.step(), iter_before2.step(), iter_after.step(), iter_after2.step()/*, ++i*/) {
				*iter_after.elem = _step_2D(*iter_before.elem, dt,  // element of first row
					*iter_before.left, *iter_before.right,
					*iter_before.elem, *iter_before.down);
				*iter_after2.elem = _step_2D(*iter_before2.elem, dt,  // element of last row
					*iter_before2.left, *iter_before2.right,
					*iter_before2.up, *iter_before2.elem);
			}
			*iter_after.elem = _step_2D(*iter_before.elem, dt,  // top right
				*iter_before.left, *iter_before.elem,
				*iter_before.elem, *iter_before.down);
			*iter_after2.elem = _step_2D(*iter_before2.elem, dt,  // bottom right
				*iter_before2.left, *iter_before2.elem,
				*iter_before2.up, *iter_before2.elem);
		}

		for (/*j = 1, */iter_before.stepRow(), iter_after.stepRow(); iter_before.rowDown < iter_before.end; iter_before.stepRow(), iter_after.stepRow()/*, ++j*/) {  // inner rows
			iter_before.jumpElem(0), iter_after.jumpElem(0);
			*iter_after.elem = _step_2D(*iter_before.elem, dt,  // start of row
				*iter_before.elem, *iter_before.right,
				*iter_before.up, *iter_before.down);
			for (/*i = 1, */iter_before.step(), iter_after.step(); iter_before.right < iter_before.rowDown; iter_before.step(), iter_after.step()/*, ++i*/) {  // inner elements of row
				*iter_after.elem = _step_2D(*iter_before.elem, dt,
					*iter_before.left, *iter_before.right,
					*iter_before.up, *iter_before.down);
			}
			*iter_after.elem = _step_2D(*iter_before.elem, dt,  // end of row
				*iter_before.left, *iter_before.elem,
				*iter_before.up, *iter_before.down);
		}
		break;
	case 3:
		/*scaling.lookup(after, 0, 0, 0) += D * (
			2 * (scaling.lookup(before, 1, 0, 0) - scaling.lookup(before, 0, 0, 0)) * _Cx +
			2 * (scaling.lookup(before, 0, 1, 0) - scaling.lookup(before, 0, 0, 0)) * _Cy +
			2 * (scaling.lookup(before, 0, 0, 1) - scaling.lookup(before, 0, 0, 0)) * _Cz
		) * dt;
		scaling.lookup(after, scaling.Nx - 1, 0, 0) += D * (
			2 * (scaling.lookup(before, scaling.Nx - 2, 0, 0) - scaling.lookup(before, scaling.Nx - 1, 0, 0)) * _Cx +
			2 * (scaling.lookup(before, scaling.Nx - 1, 1, 0) - scaling.lookup(before, scaling.Nx - 1, 0, 0)) * _Cy +
			2 * (scaling.lookup(before, scaling.Nx - 1, 0, 1) - scaling.lookup(before, scaling.Nx - 1, 0, 0)) * _Cz
		) * dt;
		scaling.lookup(after, 0, scaling.Ny - 1, 0) += D * (
			2 * (scaling.lookup(before, 1, scaling.Ny - 1, 0) - scaling.lookup(before, i, j, k)) * _Cx +
			2 * (scaling.lookup(before, i, j - 1, k) - scaling.lookup(before, i, j, k)) * _Cy +
			2 * (scaling.lookup(before, i, j, k - 1) - scaling.lookup(before, i, j, k)) * _Cz
		) * dt;
		scaling.lookup(after, scaling.Nx - 1, scaling.Ny - 1, 0) += D * (
			2 * (scaling.lookup(before, i - 1, j, k) - scaling.lookup(before, i, j, k)) * _Cx +
			2 * (scaling.lookup(before, i, j - 1, k) - scaling.lookup(before, i, j, k)) * _Cy +
			2 * (scaling.lookup(before, i, j, k - 1) - scaling.lookup(before, i, j, k)) * _Cz
		) * dt;
		scaling.lookup(after, 0, 0, scaling.Ny - 1) += D * (
			2 * (scaling.lookup(before, i - 1, j, k) - scaling.lookup(before, i, j, k)) * _Cx +
			2 * (scaling.lookup(before, i, j - 1, k) - scaling.lookup(before, i, j, k)) * _Cy +
			2 * (scaling.lookup(before, i, j, k - 1) - scaling.lookup(before, i, j, k)) * _Cz
		) * dt;
		scaling.lookup(after, scaling.Nx - 1, 0, scaling.Nz - 1) += D * (
			2 * (scaling.lookup(before, i - 1, j, k) - scaling.lookup(before, i, j, k)) * _Cx +
			2 * (scaling.lookup(before, i, j - 1, k) - scaling.lookup(before, i, j, k)) * _Cy +
			2 * (scaling.lookup(before, i, j, k - 1) - scaling.lookup(before, i, j, k)) * _Cz
		) * dt;
		scaling.lookup(after, 0, scaling.Ny - 1, scaling.Nz - 1) += D * (
			2 * (scaling.lookup(before, i - 1, j, k) - scaling.lookup(before, i, j, k)) * _Cx +
			2 * (scaling.lookup(before, i, j - 1, k) - scaling.lookup(before, i, j, k)) * _Cy +
			2 * (scaling.lookup(before, i, j, k - 1) - scaling.lookup(before, i, j, k)) * _Cz
		) * dt;
		scaling.lookup(after, scaling.Nx - 1, scaling.Ny - 1, scaling.Nz - 1) += D * (
			2 * (scaling.lookup(before, i - 1, j, k) - scaling.lookup(before, i, j, k)) * _Cx +
			2 * (scaling.lookup(before, i, j - 1, k) - scaling.lookup(before, i, j, k)) * _Cy +
			2 * (scaling.lookup(before, i, j, k - 1) - scaling.lookup(before, i, j, k)) * _Cz
		) * dt;
		for (size_t i = 1; i < scaling.Nx; ++i) {
			scaling.lookup(after, i, 0, 0) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, i, scaling.Ny - 1, 0) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, i, 0, scaling.Nz - 1) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, i, scaling.Ny - 1, scaling.Nz - 1) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
		}
		for (size_t j = 1; j < scaling.Ny - 1; ++j) {
			scaling.lookup(after, 0, j, 0) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, scaling.Nx - 1, j, 0) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, 0, j, scaling.Nz - 1) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, scaling.Nx - 1, j, scaling.Nz - 1) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			for (size_t i = 1; i < scaling.Nx - 1; ++i) {
				scaling.lookup(after, i, j, 0) += D * (
					(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
					(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
					(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
				) * dt;
				scaling.lookup(after, i, j, scaling.Nz - 1) += D * (
					(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
					(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
					(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
				) * dt;
			}
		}
		for (size_t k = 1; k < scaling.Nz - 1; ++k) {
			scaling.lookup(after, 0, 0, k) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, scaling.Nx - 1, 0, k) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, 0, scaling.Ny - 1, k) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			scaling.lookup(after, scaling.Nx - 1, scaling.Ny - 1, k) += D * (
				(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
				(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
				(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
			) * dt;
			for (size_t i = 1; i < scaling.Nx - 1; ++i) {
				scaling.lookup(after, i, 0, k) += D * (

				) * dt;
				scaling.lookup(after, i, scaling.Ny - 1, k) += D * (
					
				) * dt;
			}
			for (size_t j = 1; j < scaling.Ny - 1; ++j) {
				scaling.lookup(after, 0, j, k) += D * (

				) * dt;
				scaling.lookup(after, scaling.Nx - 1, j, k) += D * (
					
				) * dt;
				for (size_t i = 1; i < scaling.Nx - 1; ++i) {
					scaling.lookup(after, i, j, k) += D * (
						(scaling.lookup(before, i - 1, j, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i + 1, j, k)) * _Cx +
						(scaling.lookup(before, i, j - 1, k) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j + 1, k)) * _Cy +
						(scaling.lookup(before, i, j, k - 1) - 2 * scaling.lookup(before, i, j, k) + scaling.lookup(before, i, j, k + 1)) * _Cz
					) * dt;
				}
			}
		}*/
		break;
	}
}


bool CLDiffusion::parametrizeFixedBC(const void* params[], const Scaling& scaling, double dt,
	cl_context context, cl_mem init, cl_mem other,
	vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven)
{
	double D = *(const double*)params[0];
	return parametrizeFixedBC(D, scaling, dt, context, init, other,
		kernels2Precond, kernels2StepOdd, kernels2StepEven);
}

bool CLDiffusion::parametrizeFixedBC(double D, const Scaling& scaling, double dt,
	cl_context context, cl_mem init, cl_mem other,
	vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven)
{
	static cl_program program = NULL;
	if (!program && !compileCLFile(context, "step-diffusion_cell-by-cell.cl", program)) {
		program = NULL;
		return 1;
	}
	double D_times_dt = D * dt;
	union { cl_double dim1; cl_double2 dim2; cl_double3 dim3; } coeffs;  // let's handle 1D, 2D and 3D coeffs at the same host-memory location
	union { cl_uint dim1; cl_uint2 dim2; cl_uint3 dim3; } size;
	union { cl_long2 dim1; cl_long4 dim2; cl_long8 dim3; } neighbours;  // location differences between neighbours and current cell
	FloatHelper coeffHelper1D;
	Float2Helper coeffHelper2D;
	Float3Helper coeffHelper3D;
	KernelWithRange kwr;
	kwr.dim = scaling.DIM;
	kwr.offs[0] = 1;
	kwr.offs[1] = 1;
	kwr.offs[2] = 1;
	kwr.size[0] = scaling.Nx - 2;
	kwr.size[1] = scaling.Ny - 2;
	kwr.size[2] = scaling.Nz - 2;
	switch (kwr.dim)
	{
	case 1:
		coeffs.dim1 = D_times_dt / pow(scaling.dx(), 2);  // simple euler
		coeffHelper1D.value(coeffs.dim1);
		size.dim1 = scaling.Nx;
		neighbours.dim1 = { -1, 1 };
		if (!getKernelWithArgs(program, "calc_cell_1D", kwr.kernel, 5,
			sizeof(other), &other,
			sizeof(init), &init,
			coeffHelper1D.size, coeffHelper1D.ptr,
			sizeof(size.dim1), &size.dim1,
			sizeof(neighbours.dim1), &neighbours.dim1
		)) return 2;
		kernels2StepOdd.push_back(kwr);
		if (!getKernelWithArgs(program, "calc_cell_1D", kwr.kernel, 5,
			sizeof(init), &init,
			sizeof(other), &other,
			coeffHelper1D.size, coeffHelper1D.ptr,
			sizeof(size.dim1), &size.dim1,
			sizeof(neighbours.dim1), &neighbours.dim1
		)) return 2;
		kernels2StepEven.push_back(kwr);
		break;

	case 2:
		coeffs.dim2.x = D_times_dt / pow(scaling.dx(), 2);  // simple euler
		coeffs.dim2.y = D_times_dt / pow(scaling.dy(), 2);
		coeffHelper2D.value(coeffs.dim2);
		size.dim2.x = scaling.Nx;
		size.dim2.y = scaling.Ny;
		neighbours.dim2 = {
			-1, 1,  // x neighbours diff
			-(cl_long)scaling.Nx, (cl_long)scaling.Nx  // y neighbours diff
		};
		if (!getKernelWithArgs(program, "calc_cell_2D", kwr.kernel, 5,
			sizeof(other), &other,
			sizeof(init), &init,
			coeffHelper2D.size, coeffHelper2D.ptr,
			sizeof(size.dim2), &size.dim2,
			sizeof(neighbours.dim2), &neighbours.dim2
		)) return 2;
		kernels2StepOdd.push_back(kwr);
		if (!getKernelWithArgs(program, "calc_cell_2D", kwr.kernel, 5,
			sizeof(init), &init,
			sizeof(other), &other,
			coeffHelper2D.size, coeffHelper2D.ptr,
			sizeof(size.dim2), &size.dim2,
			sizeof(neighbours.dim2), &neighbours.dim2
		)) return 2;
		kernels2StepEven.push_back(kwr);
		break;

	case 3:
		coeffs.dim3.x = D_times_dt / pow(scaling.dx(), 2);  // simple Euler
		coeffs.dim3.y = D_times_dt / pow(scaling.dy(), 2);
		coeffs.dim3.z = D_times_dt / pow(scaling.dz(), 2);
		coeffHelper3D.value(coeffs.dim3);
		size.dim3.x = scaling.Nx;
		size.dim3.y = scaling.Ny;
		size.dim3.z = scaling.Nz;
		neighbours.dim3 = {
			-1, 1,  // x neighbours diff
			-(cl_long)scaling.Nx, (cl_long)scaling.Nx,  // y neighbours diff
			-(cl_long)(scaling.Nx * scaling.Ny), (cl_long)(scaling.Nx * scaling.Ny),  // z neighbours diff
		};
		if (!getKernelWithArgs(program, "calc_cell_3D", kwr.kernel, 5,
			sizeof(other), &other,
			sizeof(init), &init,
			coeffHelper3D.size, coeffHelper3D.ptr,
			sizeof(size.dim3), &size.dim3,
			sizeof(neighbours.dim3), &neighbours.dim3
		)) return 2;
		kernels2StepOdd.push_back(kwr);
		if (!getKernelWithArgs(program, "calc_cell_3D", kwr.kernel, 5,
			sizeof(init), &init,
			sizeof(other), &other,
			coeffHelper3D.size, coeffHelper3D.ptr,
			sizeof(size.dim3), &size.dim3,
			sizeof(neighbours.dim3), &neighbours.dim3
		)) return 2;
		kernels2StepEven.push_back(kwr);
		break;

	default:
		cerr << "Unsupported number of dimensions" << endl;
		return 4;
	}
	return 0;
}

bool CLDiffusion::CNe::parametrizeFixedBC(const void* params[], const Scaling& scaling, double dt,
	cl_context context, cl_mem init, cl_mem other,
	vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven)
{
	double D = *(const double*)params[0];
	return parametrizeFixedBC(D, scaling, dt, context, init, other,
		kernels2Precond, kernels2StepOdd, kernels2StepEven);
}

bool CLDiffusion::CNe::parametrizeFixedBC(double D, const Scaling& scaling, double dt,
	cl_context context, cl_mem init, cl_mem other,
	vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven)
{
	static cl_program program = NULL;
	if (!program && !compileCLFile(context, "step-diffusion_cell-by-cell.cl", program)) {
		program = NULL;
		return 1;
	}
	double D_times_dt = D * dt;
	double k_square, expramp;
	union { cl_double dim1; cl_double2 dim2; cl_double3 dim3; } coeffs;  // let's handle 1D, 2D and 3D coeffs at the same host-memory location
	union { cl_uint dim1; cl_uint2 dim2; cl_uint3 dim3; } size;
	union { cl_long2 dim1; cl_long4 dim2; cl_long8 dim3; } neighbours;  // location differences between neighbours and current cell
	FloatHelper coeffHelper1D;
	Float2Helper coeffHelper2D;
	Float3Helper coeffHelper3D;
	KernelWithRange kwr;
	kwr.dim = scaling.DIM;
	kwr.offs[0] = 1;
	kwr.offs[1] = 1;
	kwr.offs[2] = 1;
	kwr.size[0] = scaling.Nx - 2;
	kwr.size[1] = scaling.Ny - 2;
	kwr.size[2] = scaling.Nz - 2;
	switch (kwr.dim)
	{
	case 1:
		//k_square = 1 / pow(scaling.dx(), 2);
		//expramp = (1 - exp(-2 * D_times_dt * k_square)) / (2 * k_square);
		//coeffs.dim1 = expramp / pow(scaling.dx(), 2);
		coeffs.dim1 = 0.5 * (1 - (exp(-2 * D_times_dt / pow(scaling.dx(), 2))));
		coeffHelper1D.value(coeffs.dim1);
		size.dim1 = scaling.Nx;
		neighbours.dim1 = { -1, 1 };
		if (!getKernelWithArgs(program, "calc_cell_1D", kwr.kernel, 5,
			sizeof(other), &other,
			sizeof(init), &init,
			coeffHelper1D.size, coeffHelper1D.ptr,
			sizeof(size.dim1), &size.dim1,
			sizeof(neighbours.dim1), &neighbours.dim1
		)) return 2;
		kernels2StepOdd.push_back(kwr);
		if (!getKernelWithArgs(program, "calc_cell_1D", kwr.kernel, 5,
			sizeof(init), &init,
			sizeof(other), &other,
			coeffHelper1D.size, coeffHelper1D.ptr,
			sizeof(size.dim1), &size.dim1,
			sizeof(neighbours.dim1), &neighbours.dim1
		)) return 2;
		kernels2StepEven.push_back(kwr);
		break;

	case 2:
		k_square = 1 / pow(scaling.dx(), 2) + 1 / pow(scaling.dy(), 2);
		expramp = (1 - exp(-2 * D_times_dt * k_square)) / (2 * k_square);
		coeffs.dim2.x = expramp / pow(scaling.dx(), 2);
		coeffs.dim2.y = expramp / pow(scaling.dy(), 2);
		coeffHelper2D.value(coeffs.dim2);
		size.dim2.x = scaling.Nx;
		size.dim2.y = scaling.Ny;
		neighbours.dim2 = {
			-1, 1,  // x neighbours diff
			-(cl_long)scaling.Nx, (cl_long)scaling.Nx  // y neighbours diff
		};
		if (!getKernelWithArgs(program, "calc_cell_2D", kwr.kernel, 5,
			sizeof(other), &other,
			sizeof(init), &init,
			coeffHelper2D.size, coeffHelper2D.ptr,
			sizeof(size.dim2), &size.dim2,
			sizeof(neighbours.dim2), &neighbours.dim2
		)) return 2;
		kernels2StepOdd.push_back(kwr);
		if (!getKernelWithArgs(program, "calc_cell_2D", kwr.kernel, 5,
			sizeof(init), &init,
			sizeof(other), &other,
			coeffHelper2D.size, coeffHelper2D.ptr,
			sizeof(size.dim2), &size.dim2,
			sizeof(neighbours.dim2), &neighbours.dim2
		)) return 2;
		kernels2StepEven.push_back(kwr);
		break;

	case 3:
		k_square = 1 / pow(scaling.dx(), 2) + 1 / pow(scaling.dy(), 2) + 1 / pow(scaling.dz(), 2);
		expramp = (1 - exp(-2 * D_times_dt * k_square)) / (2 * k_square);
		coeffs.dim3.x = expramp / pow(scaling.dx(), 2);
		coeffs.dim3.y = expramp / pow(scaling.dy(), 2);
		coeffs.dim3.z = expramp / pow(scaling.dz(), 2);
		coeffHelper3D.value(coeffs.dim3);
		size.dim3.x = scaling.Nx;
		size.dim3.y = scaling.Ny;
		size.dim3.z = scaling.Nz;
		neighbours.dim3 = {
			-1, 1,  // x neighbours diff
			-(cl_long)scaling.Nx, (cl_long)scaling.Nx,  // y neighbours diff
			-(cl_long)(scaling.Nx * scaling.Ny), (cl_long)(scaling.Nx * scaling.Ny),  // z neighbours diff
		};
		if (!getKernelWithArgs(program, "calc_cell_3D", kwr.kernel, 5,
			sizeof(other), &other,
			sizeof(init), &init,
			coeffHelper3D.size, coeffHelper3D.ptr,
			sizeof(size.dim3), &size.dim3,
			sizeof(neighbours.dim3), &neighbours.dim3
		)) return 2;
		kernels2StepOdd.push_back(kwr);
		if (!getKernelWithArgs(program, "calc_cell_3D", kwr.kernel, 5,
			sizeof(init), &init,
			sizeof(other), &other,
			coeffHelper3D.size, coeffHelper3D.ptr,
			sizeof(size.dim3), &size.dim3,
			sizeof(neighbours.dim3), &neighbours.dim3
		)) return 2;
		kernels2StepEven.push_back(kwr);
		break;

	default:
		cerr << "Unsupported number of dimensions" << endl;
		return 4;
	}
	return 0;
}
