#pragma once

#ifdef DIFFUSIONLIB_EXPORTS
	#define DIFFUSIONLIB_API __declspec(dllexport)
#else
	#define DIFFUSIONLIB_API __declspec(dllimport)
#endif

#include "methodics.h"


class DIFFUSIONLIB_API Diffusion : public Stepper {
protected:
	double _d = NAN;
	double _Cx = NAN, _Cy = NAN, _Cz = NAN;
	void _config();
	inline Diffusion(double d, const Scaling& scaling) : Stepper(scaling), _d(d) { _config(); }
	inline Diffusion(const Diffusion &other) : Stepper(other), _d(other._d), _Cx(other._Cx), _Cy(other._Cy), _Cz(other._Cz) {}
	inline double _step_1D(const double &before, const double &dt, const double &left, const double &right) const;
	inline double _step_2D(const double &before, const double &dt,
		const double &left, const double &right, const double &up, const double &down) const;
	inline double _step_3D(const double &before, const double &dt,
		const double &left, const double &right, const double &up, const double &down, const double &above, const double &below) const;
public:
	double &D = _d;
	inline void config(double d, const Scaling& scaling) { _scaling = scaling; _d = d; _config(); }
	inline Diffusion& operator=(const Diffusion &rhs) { Stepper::operator=(rhs); _d = rhs._d; _Cx = rhs._Cx; _Cy = rhs._Cy; _Cz = rhs._Cz; return *this; }

	class FixedBC;
	class Periodic;
	class Isolated;
};

class DIFFUSIONLIB_API Diffusion::FixedBC : public Diffusion {
public:
	inline FixedBC(double d, const Scaling& scaling) : Diffusion(d, scaling) {}
	void precond(const double *raw, double *prec) const;
	void operator()(const double *before, double dt, double *after) const;
};

class DIFFUSIONLIB_API Diffusion::Periodic : public Diffusion {
public:
	inline Periodic(double d, const Scaling& scaling) : Diffusion(d, scaling) {}
	void precond(const double *raw, double *prec) const;
	void operator()(const double *before, double dt, double *after) const;
};

class DIFFUSIONLIB_API Diffusion::Isolated : public Diffusion {
public:
	inline Isolated(double d, const Scaling& scaling) : Diffusion(d, scaling) {}
	void precond(const double *raw, double *prec) const;
	void operator()(const double *before, double dt, double *after) const;
};


class DIFFUSIONLIB_API CLDiffusion {
public:
	static bool parametrizeFixedBC(const void* params[], const Scaling& scaling, double dt,
		cl_context context, cl_mem init, cl_mem other,
		vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven);
	static bool parametrizeFixedBC(double D, const Scaling& scaling, double dt,
		cl_context context, cl_mem init, cl_mem other,
		vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven);

	class CNe;
};

class DIFFUSIONLIB_API CLDiffusion::CNe {
public:
	static bool parametrizeFixedBC(const void* params[], const Scaling& scaling, double dt,
		cl_context context, cl_mem init, cl_mem other,
		vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven);
	static bool parametrizeFixedBC(double D, const Scaling& scaling, double dt,
		cl_context context, cl_mem init, cl_mem other,
		vector<KernelWithRange>& kernels2Precond, vector<KernelWithRange>& kernels2StepOdd, vector<KernelWithRange>& kernels2StepEven);
};
