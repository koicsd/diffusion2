#include "stdafx.h"
#include "methodics.h"
#include "datagrid.h"
#include "CLHelper.h"


inline void swap(double *&left, double *&right) {
	double *temp;
	temp = left;
	left = right;
	right = temp;
}

bool euler(double *&data, const Stepper &stepper, size_t nt, double t_init, double t_fin) {
	Datagrid aux;
	if (allocate(aux, stepper.scaling))
		return true;
	double dt = (t_fin - t_init) / nt;
	stepper.precond(data, aux.data);
	swap(data, aux.data);
	for (size_t t = 0; t < nt; ++t) {
		stepper(data, dt, aux.data);
		swap(data, aux.data);
	}
	release(aux);
	return false;
}


bool euler(Datagrid& grid, CLParametrizer parametrizeCL, const void* params[], size_t Nt, double t_init, double t_fin) {
	cl_context context = NULL;
	cl_command_queue queue = NULL;
	if (!initCL(context, queue)) {
		return true;
	}
	FloatArrayHelper dataHelper(grid.data, grid.scaling.nr_tot());
	cl_mem buf1 = NULL, buf2 = NULL;
	if (!createMemBuffCopyHost(context, dataHelper.size, dataHelper.ptr, buf1) ||
		!createMemBuffFromDevToHost(context, dataHelper.size, buf2)) {
		releaseResources();
		return true;
	}
	double dt = (t_fin - t_init) / Nt;
	vector<KernelWithRange> kernels2Precond, kernels2StepOdd, kernels2StepEven;
	if (parametrizeCL(params, grid.scaling, dt,
		context, buf1, buf2,
		kernels2Precond, kernels2StepOdd, kernels2StepEven)) {
		releaseResources();
		return true;
	}
	cl_mem src = buf1, dest = buf2;
	if (!enqueueCopyBuffer(queue, src, dest, dataHelper.size)) {
		releaseResources();
		return true;
	}
	vector<KernelWithRange>* pKernels = NULL;
	for (size_t nt = 0; nt <= Nt; ++nt) {
		if (pKernels == &kernels2StepOdd) {
			pKernels = &kernels2StepEven;
			src = buf1;
			dest = buf2;
		}
		else if (pKernels == &kernels2StepEven || pKernels == &kernels2Precond) {
			pKernels = &kernels2StepOdd;
			src = buf2;
			dest = buf1;
		}
		else {
			pKernels = &kernels2Precond;
			src = buf1;
			dest = buf2;
		}
		for (KernelWithRange& kwr : *pKernels)
		{
			if (!enqueueKernel(queue, kwr.kernel, kwr.dim, kwr.offs, kwr.size)) {
				releaseResources();
				return true;
			}
		}
	}
	readResult(queue, dest, dataHelper.size, dataHelper.ptr);
	dataHelper.readback(grid.data);
	releaseResources();
	return false;
}
